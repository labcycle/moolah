//! Moolah provides data types to manipulate and operate on amounts of money in various currencies.

pub mod currency;
pub mod money;

mod error;

pub use mudra::Mudra;

pub use self::error::MismatchError;

use core::{borrow::Borrow, cmp::Ordering, num::NonZeroU64};

use rust_decimal::Decimal;

use self::{currency::*, money::Money};

/// Enum to hold a [`Money`] without specifying any generic type.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Moolah {
	AED(Money<currency::AED>),
	ARS(Money<currency::ARS>),
	AUD(Money<currency::AUD>),
	BGN(Money<currency::BGN>),
	BHD(Money<currency::BHD>),
	BOV(Money<currency::BOV>),
	BRL(Money<currency::BRL>),
	CAD(Money<currency::CAD>),
	CHF(Money<currency::CHF>),
	CLP(Money<currency::CLP>),
	CNY(Money<currency::CNY>),
	COP(Money<currency::COP>),
	CZK(Money<currency::CZK>),
	DKK(Money<currency::DKK>),
	EUR(Money<currency::EUR>),
	GBP(Money<currency::GBP>),
	HKD(Money<currency::HKD>),
	HUF(Money<currency::HUF>),
	IDR(Money<currency::IDR>),
	ILS(Money<currency::ILS>),
	INR(Money<currency::INR>),
	JPY(Money<currency::JPY>),
	KRW(Money<currency::KRW>),
	MXN(Money<currency::MXN>),
	MYR(Money<currency::MYR>),
	NOK(Money<currency::NOK>),
	NZD(Money<currency::NZD>),
	PEN(Money<currency::PEN>),
	PHP(Money<currency::PHP>),
	PLN(Money<currency::PLN>),
	RON(Money<currency::RON>),
	RUB(Money<currency::RUB>),
	SAR(Money<currency::SAR>),
	SEK(Money<currency::SEK>),
	SGD(Money<currency::SGD>),
	THB(Money<currency::THB>),
	TRY(Money<currency::TRY>),
	TWD(Money<currency::TWD>),
	USD(Money<currency::USD>),
	ZAR(Money<currency::ZAR>),
}

impl Moolah {
	/// Creates [`Moolah`] from `mudra`, `units` and `nanos`.
	/// Returns [None] when `nanos` exceeds [MAX_NANOS](Money::MAX_NANOS).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah, Mudra};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let valid_moolah = Moolah::try_new(Mudra::USD, 100, 999_999_999)?;
	///
	/// assert_eq!(valid_moolah, Moolah::USD(Money::try_new(100, 999_999_999)?));
	/// assert_eq!(valid_moolah.mudra(), Mudra::USD);
	/// assert_eq!(valid_moolah.units(), 100);
	/// assert_eq!(valid_moolah.nanos(), 999_999_999);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah, Mudra};
	/// #
	/// let invalid_moolah = Moolah::try_new(Mudra::USD, 100, 1_000_000_000);
	///
	/// assert_eq!(invalid_moolah, None);
	/// ```
	pub fn try_new<M>(mudra: M, units: u64, nanos: u32) -> Option<Self>
	where
		M: Borrow<Mudra>,
	{
		let moolah = match mudra.borrow() {
			Mudra::AED => Self::AED(Money::try_new(units, nanos)?),
			Mudra::ARS => Self::ARS(Money::try_new(units, nanos)?),
			Mudra::AUD => Self::AUD(Money::try_new(units, nanos)?),
			Mudra::BGN => Self::BGN(Money::try_new(units, nanos)?),
			Mudra::BHD => Self::BHD(Money::try_new(units, nanos)?),
			Mudra::BOV => Self::BOV(Money::try_new(units, nanos)?),
			Mudra::BRL => Self::BRL(Money::try_new(units, nanos)?),
			Mudra::CAD => Self::CAD(Money::try_new(units, nanos)?),
			Mudra::CHF => Self::CHF(Money::try_new(units, nanos)?),
			Mudra::CLP => Self::CLP(Money::try_new(units, nanos)?),
			Mudra::CNY => Self::CNY(Money::try_new(units, nanos)?),
			Mudra::COP => Self::COP(Money::try_new(units, nanos)?),
			Mudra::CZK => Self::CZK(Money::try_new(units, nanos)?),
			Mudra::DKK => Self::DKK(Money::try_new(units, nanos)?),
			Mudra::EUR => Self::EUR(Money::try_new(units, nanos)?),
			Mudra::GBP => Self::GBP(Money::try_new(units, nanos)?),
			Mudra::HKD => Self::HKD(Money::try_new(units, nanos)?),
			Mudra::HUF => Self::HUF(Money::try_new(units, nanos)?),
			Mudra::IDR => Self::IDR(Money::try_new(units, nanos)?),
			Mudra::ILS => Self::ILS(Money::try_new(units, nanos)?),
			Mudra::INR => Self::INR(Money::try_new(units, nanos)?),
			Mudra::JPY => Self::JPY(Money::try_new(units, nanos)?),
			Mudra::KRW => Self::KRW(Money::try_new(units, nanos)?),
			Mudra::MXN => Self::MXN(Money::try_new(units, nanos)?),
			Mudra::MYR => Self::MYR(Money::try_new(units, nanos)?),
			Mudra::NOK => Self::NOK(Money::try_new(units, nanos)?),
			Mudra::NZD => Self::NZD(Money::try_new(units, nanos)?),
			Mudra::PEN => Self::PEN(Money::try_new(units, nanos)?),
			Mudra::PHP => Self::PHP(Money::try_new(units, nanos)?),
			Mudra::PLN => Self::PLN(Money::try_new(units, nanos)?),
			Mudra::RON => Self::RON(Money::try_new(units, nanos)?),
			Mudra::RUB => Self::RUB(Money::try_new(units, nanos)?),
			Mudra::SAR => Self::SAR(Money::try_new(units, nanos)?),
			Mudra::SEK => Self::SEK(Money::try_new(units, nanos)?),
			Mudra::SGD => Self::SGD(Money::try_new(units, nanos)?),
			Mudra::THB => Self::THB(Money::try_new(units, nanos)?),
			Mudra::TRY => Self::TRY(Money::try_new(units, nanos)?),
			Mudra::TWD => Self::TWD(Money::try_new(units, nanos)?),
			Mudra::USD => Self::USD(Money::try_new(units, nanos)?),
			Mudra::ZAR => Self::ZAR(Money::try_new(units, nanos)?),
		};

		Some(moolah)
	}

	/// Changes the associated currency i.e. [Mudra].
	/// The returned [`Moolah`] references the original amount.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::EUR, money::Money, Moolah, Mudra};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let usd_money = Money::try_new(42, 1000)?;
	/// let usd_moolah = Moolah::USD(usd_money.clone());
	///
	/// let eur_moolah = usd_moolah.with_mudra(Mudra::EUR);
	///
	/// assert_eq!(eur_moolah.mudra(), Mudra::EUR);
	/// assert_eq!(eur_moolah, Moolah::EUR(usd_money.with_currency::<EUR>()));
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn with_mudra<M>(&self, new_mudra: M) -> Self
	where
		M: Borrow<Mudra>,
	{
		let new_mudra = new_mudra.borrow();

		return match self {
			Moolah::AED(money) => create_moolah(money, new_mudra),
			Moolah::ARS(money) => create_moolah(money, new_mudra),
			Moolah::AUD(money) => create_moolah(money, new_mudra),
			Moolah::BGN(money) => create_moolah(money, new_mudra),
			Moolah::BHD(money) => create_moolah(money, new_mudra),
			Moolah::BOV(money) => create_moolah(money, new_mudra),
			Moolah::BRL(money) => create_moolah(money, new_mudra),
			Moolah::CAD(money) => create_moolah(money, new_mudra),
			Moolah::CHF(money) => create_moolah(money, new_mudra),
			Moolah::CLP(money) => create_moolah(money, new_mudra),
			Moolah::CNY(money) => create_moolah(money, new_mudra),
			Moolah::COP(money) => create_moolah(money, new_mudra),
			Moolah::CZK(money) => create_moolah(money, new_mudra),
			Moolah::DKK(money) => create_moolah(money, new_mudra),
			Moolah::EUR(money) => create_moolah(money, new_mudra),
			Moolah::GBP(money) => create_moolah(money, new_mudra),
			Moolah::HKD(money) => create_moolah(money, new_mudra),
			Moolah::HUF(money) => create_moolah(money, new_mudra),
			Moolah::IDR(money) => create_moolah(money, new_mudra),
			Moolah::ILS(money) => create_moolah(money, new_mudra),
			Moolah::INR(money) => create_moolah(money, new_mudra),
			Moolah::JPY(money) => create_moolah(money, new_mudra),
			Moolah::KRW(money) => create_moolah(money, new_mudra),
			Moolah::MXN(money) => create_moolah(money, new_mudra),
			Moolah::MYR(money) => create_moolah(money, new_mudra),
			Moolah::NOK(money) => create_moolah(money, new_mudra),
			Moolah::NZD(money) => create_moolah(money, new_mudra),
			Moolah::PEN(money) => create_moolah(money, new_mudra),
			Moolah::PHP(money) => create_moolah(money, new_mudra),
			Moolah::PLN(money) => create_moolah(money, new_mudra),
			Moolah::RON(money) => create_moolah(money, new_mudra),
			Moolah::RUB(money) => create_moolah(money, new_mudra),
			Moolah::SAR(money) => create_moolah(money, new_mudra),
			Moolah::SEK(money) => create_moolah(money, new_mudra),
			Moolah::SGD(money) => create_moolah(money, new_mudra),
			Moolah::THB(money) => create_moolah(money, new_mudra),
			Moolah::TRY(money) => create_moolah(money, new_mudra),
			Moolah::TWD(money) => create_moolah(money, new_mudra),
			Moolah::USD(money) => create_moolah(money, new_mudra),
			Moolah::ZAR(money) => create_moolah(money, new_mudra),
		};

		fn create_moolah<C>(money: &Money<C>, mudra: &Mudra) -> Moolah {
			match mudra {
				Mudra::AED => Moolah::AED(money.with_currency()),
				Mudra::ARS => Moolah::ARS(money.with_currency()),
				Mudra::AUD => Moolah::AUD(money.with_currency()),
				Mudra::BGN => Moolah::BGN(money.with_currency()),
				Mudra::BHD => Moolah::BHD(money.with_currency()),
				Mudra::BOV => Moolah::BOV(money.with_currency()),
				Mudra::BRL => Moolah::BRL(money.with_currency()),
				Mudra::CAD => Moolah::CAD(money.with_currency()),
				Mudra::CHF => Moolah::CHF(money.with_currency()),
				Mudra::CLP => Moolah::CLP(money.with_currency()),
				Mudra::CNY => Moolah::CNY(money.with_currency()),
				Mudra::COP => Moolah::COP(money.with_currency()),
				Mudra::CZK => Moolah::CZK(money.with_currency()),
				Mudra::DKK => Moolah::DKK(money.with_currency()),
				Mudra::EUR => Moolah::EUR(money.with_currency()),
				Mudra::GBP => Moolah::GBP(money.with_currency()),
				Mudra::HKD => Moolah::HKD(money.with_currency()),
				Mudra::HUF => Moolah::HUF(money.with_currency()),
				Mudra::IDR => Moolah::IDR(money.with_currency()),
				Mudra::ILS => Moolah::ILS(money.with_currency()),
				Mudra::INR => Moolah::INR(money.with_currency()),
				Mudra::JPY => Moolah::JPY(money.with_currency()),
				Mudra::KRW => Moolah::KRW(money.with_currency()),
				Mudra::MXN => Moolah::MXN(money.with_currency()),
				Mudra::MYR => Moolah::MYR(money.with_currency()),
				Mudra::NOK => Moolah::NOK(money.with_currency()),
				Mudra::NZD => Moolah::NZD(money.with_currency()),
				Mudra::PEN => Moolah::PEN(money.with_currency()),
				Mudra::PHP => Moolah::PHP(money.with_currency()),
				Mudra::PLN => Moolah::PLN(money.with_currency()),
				Mudra::RON => Moolah::RON(money.with_currency()),
				Mudra::RUB => Moolah::RUB(money.with_currency()),
				Mudra::SAR => Moolah::SAR(money.with_currency()),
				Mudra::SEK => Moolah::SEK(money.with_currency()),
				Mudra::SGD => Moolah::SGD(money.with_currency()),
				Mudra::THB => Moolah::THB(money.with_currency()),
				Mudra::TRY => Moolah::TRY(money.with_currency()),
				Mudra::TWD => Moolah::TWD(money.with_currency()),
				Mudra::USD => Moolah::USD(money.with_currency()),
				Mudra::ZAR => Moolah::ZAR(money.with_currency()),
			}
		}
	}

	/// Returns `self`'s corresponding [`Mudra`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah, Mudra};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 0)?);
	///
	/// assert_eq!(moolah.mudra(), Mudra::USD);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn mudra(&self) -> Mudra {
		match self {
			Self::AED(_) => Mudra::AED,
			Self::ARS(_) => Mudra::ARS,
			Self::AUD(_) => Mudra::AUD,
			Self::BGN(_) => Mudra::BGN,
			Self::BHD(_) => Mudra::BHD,
			Self::BOV(_) => Mudra::BOV,
			Self::BRL(_) => Mudra::BRL,
			Self::CAD(_) => Mudra::CAD,
			Self::CHF(_) => Mudra::CHF,
			Self::CLP(_) => Mudra::CLP,
			Self::CNY(_) => Mudra::CNY,
			Self::COP(_) => Mudra::COP,
			Self::CZK(_) => Mudra::CZK,
			Self::DKK(_) => Mudra::DKK,
			Self::EUR(_) => Mudra::EUR,
			Self::GBP(_) => Mudra::GBP,
			Self::HKD(_) => Mudra::HKD,
			Self::HUF(_) => Mudra::HUF,
			Self::IDR(_) => Mudra::IDR,
			Self::ILS(_) => Mudra::ILS,
			Self::INR(_) => Mudra::INR,
			Self::JPY(_) => Mudra::JPY,
			Self::KRW(_) => Mudra::KRW,
			Self::MXN(_) => Mudra::MXN,
			Self::MYR(_) => Mudra::MYR,
			Self::NOK(_) => Mudra::NOK,
			Self::NZD(_) => Mudra::NZD,
			Self::PEN(_) => Mudra::PEN,
			Self::PHP(_) => Mudra::PHP,
			Self::PLN(_) => Mudra::PLN,
			Self::RON(_) => Mudra::RON,
			Self::RUB(_) => Mudra::RUB,
			Self::SAR(_) => Mudra::SAR,
			Self::SEK(_) => Mudra::SEK,
			Self::SGD(_) => Mudra::SGD,
			Self::THB(_) => Mudra::THB,
			Self::TRY(_) => Mudra::TRY,
			Self::TWD(_) => Mudra::TWD,
			Self::USD(_) => Mudra::USD,
			Self::ZAR(_) => Mudra::ZAR,
		}
	}

	/// Returns `units` of the amount in `self`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Moolah::USD(Money::try_new(42, 99)?);
	///
	/// assert_eq!(money.units(), 42);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn units(&self) -> u64 {
		match self {
			Self::AED(money) => money.units(),
			Self::ARS(money) => money.units(),
			Self::AUD(money) => money.units(),
			Self::BGN(money) => money.units(),
			Self::BHD(money) => money.units(),
			Self::BOV(money) => money.units(),
			Self::BRL(money) => money.units(),
			Self::CAD(money) => money.units(),
			Self::CHF(money) => money.units(),
			Self::CLP(money) => money.units(),
			Self::CNY(money) => money.units(),
			Self::COP(money) => money.units(),
			Self::CZK(money) => money.units(),
			Self::DKK(money) => money.units(),
			Self::EUR(money) => money.units(),
			Self::GBP(money) => money.units(),
			Self::HKD(money) => money.units(),
			Self::HUF(money) => money.units(),
			Self::IDR(money) => money.units(),
			Self::ILS(money) => money.units(),
			Self::INR(money) => money.units(),
			Self::JPY(money) => money.units(),
			Self::KRW(money) => money.units(),
			Self::MXN(money) => money.units(),
			Self::MYR(money) => money.units(),
			Self::NOK(money) => money.units(),
			Self::NZD(money) => money.units(),
			Self::PEN(money) => money.units(),
			Self::PHP(money) => money.units(),
			Self::PLN(money) => money.units(),
			Self::RON(money) => money.units(),
			Self::RUB(money) => money.units(),
			Self::SAR(money) => money.units(),
			Self::SEK(money) => money.units(),
			Self::SGD(money) => money.units(),
			Self::THB(money) => money.units(),
			Self::TRY(money) => money.units(),
			Self::TWD(money) => money.units(),
			Self::USD(money) => money.units(),
			Self::ZAR(money) => money.units(),
		}
	}

	/// Returns `nanos` of the amount in `self`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Moolah::USD(Money::try_new(42, 99)?);
	///
	/// assert_eq!(money.nanos(), 99);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn nanos(&self) -> u32 {
		match self {
			Self::AED(money) => money.nanos(),
			Self::ARS(money) => money.nanos(),
			Self::AUD(money) => money.nanos(),
			Self::BGN(money) => money.nanos(),
			Self::BHD(money) => money.nanos(),
			Self::BOV(money) => money.nanos(),
			Self::BRL(money) => money.nanos(),
			Self::CAD(money) => money.nanos(),
			Self::CHF(money) => money.nanos(),
			Self::CLP(money) => money.nanos(),
			Self::CNY(money) => money.nanos(),
			Self::COP(money) => money.nanos(),
			Self::CZK(money) => money.nanos(),
			Self::DKK(money) => money.nanos(),
			Self::EUR(money) => money.nanos(),
			Self::GBP(money) => money.nanos(),
			Self::HKD(money) => money.nanos(),
			Self::HUF(money) => money.nanos(),
			Self::IDR(money) => money.nanos(),
			Self::ILS(money) => money.nanos(),
			Self::INR(money) => money.nanos(),
			Self::JPY(money) => money.nanos(),
			Self::KRW(money) => money.nanos(),
			Self::MXN(money) => money.nanos(),
			Self::MYR(money) => money.nanos(),
			Self::NOK(money) => money.nanos(),
			Self::NZD(money) => money.nanos(),
			Self::PEN(money) => money.nanos(),
			Self::PHP(money) => money.nanos(),
			Self::PLN(money) => money.nanos(),
			Self::RON(money) => money.nanos(),
			Self::RUB(money) => money.nanos(),
			Self::SAR(money) => money.nanos(),
			Self::SEK(money) => money.nanos(),
			Self::SGD(money) => money.nanos(),
			Self::THB(money) => money.nanos(),
			Self::TRY(money) => money.nanos(),
			Self::TWD(money) => money.nanos(),
			Self::USD(money) => money.nanos(),
			Self::ZAR(money) => money.nanos(),
		}
	}

	/// Adds `rhs` to `self` and returns the sum.
	///
	/// # Returns
	///
	/// - `Ok(Some(Moolah))`: when `rhs` and `self` are the same variant of [Moolah].
	///
	/// - `Ok(None)`: when the sum exceeds [`Money::MAX`].
	///
	/// - `Err(MismatchError)`: when `rhs` and `self` are not the same variant of [Moolah].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(42, 0)?);
	/// let rhs = Moolah::USD(Money::try_new(50, 5000)?);
	///
	/// let sum = lhs.checked_add(&rhs).ok()??;
	///
	/// assert_eq!(sum, Moolah::USD(Money::try_new(92, 5000)?));
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::MAX);
	/// let rhs = Moolah::USD(Money::try_new(0, 1)?);
	///
	/// let sum = lhs.checked_add(&rhs).ok()?;
	///
	/// assert_eq!(sum, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, MismatchError, Moolah, Mudra};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::MAX);
	/// let rhs = Moolah::EUR(Money::ZERO);
	///
	/// let sum = lhs.checked_add(&rhs);
	///
	/// assert!(matches!(sum, Err(MismatchError)));
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn checked_add(&self, rhs: &Self) -> Result<Option<Self>, MismatchError> {
		let sum = || {
			let sum = match (self, rhs) {
				(Self::AED(lhs), Self::AED(rhs)) => Self::AED(lhs.checked_add(rhs)?),
				(Self::ARS(lhs), Self::ARS(rhs)) => Self::ARS(lhs.checked_add(rhs)?),
				(Self::AUD(lhs), Self::AUD(rhs)) => Self::AUD(lhs.checked_add(rhs)?),
				(Self::BGN(lhs), Self::BGN(rhs)) => Self::BGN(lhs.checked_add(rhs)?),
				(Self::BHD(lhs), Self::BHD(rhs)) => Self::BHD(lhs.checked_add(rhs)?),
				(Self::BOV(lhs), Self::BOV(rhs)) => Self::BOV(lhs.checked_add(rhs)?),
				(Self::BRL(lhs), Self::BRL(rhs)) => Self::BRL(lhs.checked_add(rhs)?),
				(Self::CAD(lhs), Self::CAD(rhs)) => Self::CAD(lhs.checked_add(rhs)?),
				(Self::CHF(lhs), Self::CHF(rhs)) => Self::CHF(lhs.checked_add(rhs)?),
				(Self::CLP(lhs), Self::CLP(rhs)) => Self::CLP(lhs.checked_add(rhs)?),
				(Self::CNY(lhs), Self::CNY(rhs)) => Self::CNY(lhs.checked_add(rhs)?),
				(Self::COP(lhs), Self::COP(rhs)) => Self::COP(lhs.checked_add(rhs)?),
				(Self::CZK(lhs), Self::CZK(rhs)) => Self::CZK(lhs.checked_add(rhs)?),
				(Self::DKK(lhs), Self::DKK(rhs)) => Self::DKK(lhs.checked_add(rhs)?),
				(Self::EUR(lhs), Self::EUR(rhs)) => Self::EUR(lhs.checked_add(rhs)?),
				(Self::GBP(lhs), Self::GBP(rhs)) => Self::GBP(lhs.checked_add(rhs)?),
				(Self::HKD(lhs), Self::HKD(rhs)) => Self::HKD(lhs.checked_add(rhs)?),
				(Self::HUF(lhs), Self::HUF(rhs)) => Self::HUF(lhs.checked_add(rhs)?),
				(Self::IDR(lhs), Self::IDR(rhs)) => Self::IDR(lhs.checked_add(rhs)?),
				(Self::ILS(lhs), Self::ILS(rhs)) => Self::ILS(lhs.checked_add(rhs)?),
				(Self::INR(lhs), Self::INR(rhs)) => Self::INR(lhs.checked_add(rhs)?),
				(Self::JPY(lhs), Self::JPY(rhs)) => Self::JPY(lhs.checked_add(rhs)?),
				(Self::KRW(lhs), Self::KRW(rhs)) => Self::KRW(lhs.checked_add(rhs)?),
				(Self::MXN(lhs), Self::MXN(rhs)) => Self::MXN(lhs.checked_add(rhs)?),
				(Self::MYR(lhs), Self::MYR(rhs)) => Self::MYR(lhs.checked_add(rhs)?),
				(Self::NOK(lhs), Self::NOK(rhs)) => Self::NOK(lhs.checked_add(rhs)?),
				(Self::NZD(lhs), Self::NZD(rhs)) => Self::NZD(lhs.checked_add(rhs)?),
				(Self::PEN(lhs), Self::PEN(rhs)) => Self::PEN(lhs.checked_add(rhs)?),
				(Self::PHP(lhs), Self::PHP(rhs)) => Self::PHP(lhs.checked_add(rhs)?),
				(Self::PLN(lhs), Self::PLN(rhs)) => Self::PLN(lhs.checked_add(rhs)?),
				(Self::RON(lhs), Self::RON(rhs)) => Self::RON(lhs.checked_add(rhs)?),
				(Self::RUB(lhs), Self::RUB(rhs)) => Self::RUB(lhs.checked_add(rhs)?),
				(Self::SAR(lhs), Self::SAR(rhs)) => Self::SAR(lhs.checked_add(rhs)?),
				(Self::SEK(lhs), Self::SEK(rhs)) => Self::SEK(lhs.checked_add(rhs)?),
				(Self::SGD(lhs), Self::SGD(rhs)) => Self::SGD(lhs.checked_add(rhs)?),
				(Self::THB(lhs), Self::THB(rhs)) => Self::THB(lhs.checked_add(rhs)?),
				(Self::TRY(lhs), Self::TRY(rhs)) => Self::TRY(lhs.checked_add(rhs)?),
				(Self::TWD(lhs), Self::TWD(rhs)) => Self::TWD(lhs.checked_add(rhs)?),
				(Self::USD(lhs), Self::USD(rhs)) => Self::USD(lhs.checked_add(rhs)?),
				(Self::ZAR(lhs), Self::ZAR(rhs)) => Self::ZAR(lhs.checked_add(rhs)?),
				_ => return Some(None),
			};

			Some(Some(sum))
		};

		match sum() {
			Some(Some(sum)) => Ok(Some(sum)),
			None => Ok(None),
			Some(None) => Err(MismatchError::new(self.mudra(), rhs.mudra())),
		}
	}

	/// Subtracts `rhs` from `self`, and returns the difference.
	///
	/// # Returns
	///
	/// - `Ok(Some(Moolah))`: when `rhs` and `self` are the same variant of [Moolah].
	///
	/// - `Ok(None)`: when the `rhs` exceeds `self`.
	///
	/// - `Err(MismatchError)`: when `rhs` and `self` are not the same variant of [Moolah].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, MismatchError, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(50, 5000)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 0)?);
	///
	/// let difference = lhs.checked_sub(&rhs).ok()??;
	///
	/// assert_eq!(difference, Moolah::USD(Money::try_new(8, 5000)?));
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(0, 1)?);
	/// let rhs = Moolah::USD(Money::try_new(1, 0)?);
	///
	/// let difference = lhs.checked_sub(&rhs).ok()?;
	///
	/// assert_eq!(difference, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, MismatchError, Moolah, Mudra};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::MAX);
	/// let rhs = Moolah::EUR(Money::ZERO);
	///
	/// let difference = lhs.checked_sub(&rhs);
	///
	/// assert!(matches!(difference, Err(MismatchError)));
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn checked_sub(&self, rhs: &Self) -> Result<Option<Self>, MismatchError> {
		let difference = || {
			let difference = match (self, rhs) {
				(Self::AED(lhs), Self::AED(rhs)) => Self::AED(lhs.checked_sub(rhs)?),
				(Self::ARS(lhs), Self::ARS(rhs)) => Self::ARS(lhs.checked_sub(rhs)?),
				(Self::AUD(lhs), Self::AUD(rhs)) => Self::AUD(lhs.checked_sub(rhs)?),
				(Self::BGN(lhs), Self::BGN(rhs)) => Self::BGN(lhs.checked_sub(rhs)?),
				(Self::BHD(lhs), Self::BHD(rhs)) => Self::BHD(lhs.checked_sub(rhs)?),
				(Self::BOV(lhs), Self::BOV(rhs)) => Self::BOV(lhs.checked_sub(rhs)?),
				(Self::BRL(lhs), Self::BRL(rhs)) => Self::BRL(lhs.checked_sub(rhs)?),
				(Self::CAD(lhs), Self::CAD(rhs)) => Self::CAD(lhs.checked_sub(rhs)?),
				(Self::CHF(lhs), Self::CHF(rhs)) => Self::CHF(lhs.checked_sub(rhs)?),
				(Self::CLP(lhs), Self::CLP(rhs)) => Self::CLP(lhs.checked_sub(rhs)?),
				(Self::CNY(lhs), Self::CNY(rhs)) => Self::CNY(lhs.checked_sub(rhs)?),
				(Self::COP(lhs), Self::COP(rhs)) => Self::COP(lhs.checked_sub(rhs)?),
				(Self::CZK(lhs), Self::CZK(rhs)) => Self::CZK(lhs.checked_sub(rhs)?),
				(Self::DKK(lhs), Self::DKK(rhs)) => Self::DKK(lhs.checked_sub(rhs)?),
				(Self::EUR(lhs), Self::EUR(rhs)) => Self::EUR(lhs.checked_sub(rhs)?),
				(Self::GBP(lhs), Self::GBP(rhs)) => Self::GBP(lhs.checked_sub(rhs)?),
				(Self::HKD(lhs), Self::HKD(rhs)) => Self::HKD(lhs.checked_sub(rhs)?),
				(Self::HUF(lhs), Self::HUF(rhs)) => Self::HUF(lhs.checked_sub(rhs)?),
				(Self::IDR(lhs), Self::IDR(rhs)) => Self::IDR(lhs.checked_sub(rhs)?),
				(Self::ILS(lhs), Self::ILS(rhs)) => Self::ILS(lhs.checked_sub(rhs)?),
				(Self::INR(lhs), Self::INR(rhs)) => Self::INR(lhs.checked_sub(rhs)?),
				(Self::JPY(lhs), Self::JPY(rhs)) => Self::JPY(lhs.checked_sub(rhs)?),
				(Self::KRW(lhs), Self::KRW(rhs)) => Self::KRW(lhs.checked_sub(rhs)?),
				(Self::MXN(lhs), Self::MXN(rhs)) => Self::MXN(lhs.checked_sub(rhs)?),
				(Self::MYR(lhs), Self::MYR(rhs)) => Self::MYR(lhs.checked_sub(rhs)?),
				(Self::NOK(lhs), Self::NOK(rhs)) => Self::NOK(lhs.checked_sub(rhs)?),
				(Self::NZD(lhs), Self::NZD(rhs)) => Self::NZD(lhs.checked_sub(rhs)?),
				(Self::PEN(lhs), Self::PEN(rhs)) => Self::PEN(lhs.checked_sub(rhs)?),
				(Self::PHP(lhs), Self::PHP(rhs)) => Self::PHP(lhs.checked_sub(rhs)?),
				(Self::PLN(lhs), Self::PLN(rhs)) => Self::PLN(lhs.checked_sub(rhs)?),
				(Self::RON(lhs), Self::RON(rhs)) => Self::RON(lhs.checked_sub(rhs)?),
				(Self::RUB(lhs), Self::RUB(rhs)) => Self::RUB(lhs.checked_sub(rhs)?),
				(Self::SAR(lhs), Self::SAR(rhs)) => Self::SAR(lhs.checked_sub(rhs)?),
				(Self::SEK(lhs), Self::SEK(rhs)) => Self::SEK(lhs.checked_sub(rhs)?),
				(Self::SGD(lhs), Self::SGD(rhs)) => Self::SGD(lhs.checked_sub(rhs)?),
				(Self::THB(lhs), Self::THB(rhs)) => Self::THB(lhs.checked_sub(rhs)?),
				(Self::TRY(lhs), Self::TRY(rhs)) => Self::TRY(lhs.checked_sub(rhs)?),
				(Self::TWD(lhs), Self::TWD(rhs)) => Self::TWD(lhs.checked_sub(rhs)?),
				(Self::USD(lhs), Self::USD(rhs)) => Self::USD(lhs.checked_sub(rhs)?),
				(Self::ZAR(lhs), Self::ZAR(rhs)) => Self::ZAR(lhs.checked_sub(rhs)?),
				_ => return Some(None),
			};

			Some(Some(difference))
		};

		match difference() {
			Some(Some(difference)) => Ok(Some(difference)),
			None => Ok(None),
			Some(None) => Err(MismatchError::new(self.mudra(), rhs.mudra())),
		}
	}

	/// Multiplies `self` by `rhs`, and returns the product.
	/// Returns [None] when the product exceeds [`Money::MAX`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(50, 550_000_000)?);
	///
	/// assert_eq!(moolah.checked_mul_by_u64(2)?, Moolah::USD(Money::try_new(101, 100_000_000)?));
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let moolah = Moolah::USD(Money::MAX);
	///
	/// assert_eq!(moolah.checked_mul_by_u64(2), None);
	/// ```
	pub fn checked_mul_by_u64(&self, rhs: u64) -> Option<Self> {
		let product = match self {
			Self::AED(money) => Self::AED(money.checked_mul_by_u64(rhs)?),
			Self::ARS(money) => Self::ARS(money.checked_mul_by_u64(rhs)?),
			Self::AUD(money) => Self::AUD(money.checked_mul_by_u64(rhs)?),
			Self::BGN(money) => Self::BGN(money.checked_mul_by_u64(rhs)?),
			Self::BHD(money) => Self::BHD(money.checked_mul_by_u64(rhs)?),
			Self::BOV(money) => Self::BOV(money.checked_mul_by_u64(rhs)?),
			Self::BRL(money) => Self::BRL(money.checked_mul_by_u64(rhs)?),
			Self::CAD(money) => Self::CAD(money.checked_mul_by_u64(rhs)?),
			Self::CHF(money) => Self::CHF(money.checked_mul_by_u64(rhs)?),
			Self::CLP(money) => Self::CLP(money.checked_mul_by_u64(rhs)?),
			Self::CNY(money) => Self::CNY(money.checked_mul_by_u64(rhs)?),
			Self::COP(money) => Self::COP(money.checked_mul_by_u64(rhs)?),
			Self::CZK(money) => Self::CZK(money.checked_mul_by_u64(rhs)?),
			Self::DKK(money) => Self::DKK(money.checked_mul_by_u64(rhs)?),
			Self::EUR(money) => Self::EUR(money.checked_mul_by_u64(rhs)?),
			Self::GBP(money) => Self::GBP(money.checked_mul_by_u64(rhs)?),
			Self::HKD(money) => Self::HKD(money.checked_mul_by_u64(rhs)?),
			Self::HUF(money) => Self::HUF(money.checked_mul_by_u64(rhs)?),
			Self::IDR(money) => Self::IDR(money.checked_mul_by_u64(rhs)?),
			Self::ILS(money) => Self::ILS(money.checked_mul_by_u64(rhs)?),
			Self::INR(money) => Self::INR(money.checked_mul_by_u64(rhs)?),
			Self::JPY(money) => Self::JPY(money.checked_mul_by_u64(rhs)?),
			Self::KRW(money) => Self::KRW(money.checked_mul_by_u64(rhs)?),
			Self::MXN(money) => Self::MXN(money.checked_mul_by_u64(rhs)?),
			Self::MYR(money) => Self::MYR(money.checked_mul_by_u64(rhs)?),
			Self::NOK(money) => Self::NOK(money.checked_mul_by_u64(rhs)?),
			Self::NZD(money) => Self::NZD(money.checked_mul_by_u64(rhs)?),
			Self::PEN(money) => Self::PEN(money.checked_mul_by_u64(rhs)?),
			Self::PHP(money) => Self::PHP(money.checked_mul_by_u64(rhs)?),
			Self::PLN(money) => Self::PLN(money.checked_mul_by_u64(rhs)?),
			Self::RON(money) => Self::RON(money.checked_mul_by_u64(rhs)?),
			Self::RUB(money) => Self::RUB(money.checked_mul_by_u64(rhs)?),
			Self::SAR(money) => Self::SAR(money.checked_mul_by_u64(rhs)?),
			Self::SEK(money) => Self::SEK(money.checked_mul_by_u64(rhs)?),
			Self::SGD(money) => Self::SGD(money.checked_mul_by_u64(rhs)?),
			Self::THB(money) => Self::THB(money.checked_mul_by_u64(rhs)?),
			Self::TRY(money) => Self::TRY(money.checked_mul_by_u64(rhs)?),
			Self::TWD(money) => Self::TWD(money.checked_mul_by_u64(rhs)?),
			Self::USD(money) => Self::USD(money.checked_mul_by_u64(rhs)?),
			Self::ZAR(money) => Self::ZAR(money.checked_mul_by_u64(rhs)?),
		};

		Some(product)
	}

	/// Divides `self` by `rhs`, and returns the quotient.
	///
	/// # Rounding
	///
	/// Rounds `nanos` using banker's rounding. For more details see [Money]'s documentation.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(6, 3000)?);
	/// let rhs = 2.try_into().ok()?;
	///
	/// assert_eq!(moolah.div_by_non_zero_u64(rhs), Moolah::USD(Money::try_new(3, 1500)?));
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn div_by_non_zero_u64(&self, rhs: NonZeroU64) -> Self {
		match self {
			Self::AED(money) => Self::AED(money.div_by_non_zero_u64(rhs)),
			Self::ARS(money) => Self::ARS(money.div_by_non_zero_u64(rhs)),
			Self::AUD(money) => Self::AUD(money.div_by_non_zero_u64(rhs)),
			Self::BGN(money) => Self::BGN(money.div_by_non_zero_u64(rhs)),
			Self::BHD(money) => Self::BHD(money.div_by_non_zero_u64(rhs)),
			Self::BOV(money) => Self::BOV(money.div_by_non_zero_u64(rhs)),
			Self::BRL(money) => Self::BRL(money.div_by_non_zero_u64(rhs)),
			Self::CAD(money) => Self::CAD(money.div_by_non_zero_u64(rhs)),
			Self::CHF(money) => Self::CHF(money.div_by_non_zero_u64(rhs)),
			Self::CLP(money) => Self::CLP(money.div_by_non_zero_u64(rhs)),
			Self::CNY(money) => Self::CNY(money.div_by_non_zero_u64(rhs)),
			Self::COP(money) => Self::COP(money.div_by_non_zero_u64(rhs)),
			Self::CZK(money) => Self::CZK(money.div_by_non_zero_u64(rhs)),
			Self::DKK(money) => Self::DKK(money.div_by_non_zero_u64(rhs)),
			Self::EUR(money) => Self::EUR(money.div_by_non_zero_u64(rhs)),
			Self::GBP(money) => Self::GBP(money.div_by_non_zero_u64(rhs)),
			Self::HKD(money) => Self::HKD(money.div_by_non_zero_u64(rhs)),
			Self::HUF(money) => Self::HUF(money.div_by_non_zero_u64(rhs)),
			Self::IDR(money) => Self::IDR(money.div_by_non_zero_u64(rhs)),
			Self::ILS(money) => Self::ILS(money.div_by_non_zero_u64(rhs)),
			Self::INR(money) => Self::INR(money.div_by_non_zero_u64(rhs)),
			Self::JPY(money) => Self::JPY(money.div_by_non_zero_u64(rhs)),
			Self::KRW(money) => Self::KRW(money.div_by_non_zero_u64(rhs)),
			Self::MXN(money) => Self::MXN(money.div_by_non_zero_u64(rhs)),
			Self::MYR(money) => Self::MYR(money.div_by_non_zero_u64(rhs)),
			Self::NOK(money) => Self::NOK(money.div_by_non_zero_u64(rhs)),
			Self::NZD(money) => Self::NZD(money.div_by_non_zero_u64(rhs)),
			Self::PEN(money) => Self::PEN(money.div_by_non_zero_u64(rhs)),
			Self::PHP(money) => Self::PHP(money.div_by_non_zero_u64(rhs)),
			Self::PLN(money) => Self::PLN(money.div_by_non_zero_u64(rhs)),
			Self::RON(money) => Self::RON(money.div_by_non_zero_u64(rhs)),
			Self::RUB(money) => Self::RUB(money.div_by_non_zero_u64(rhs)),
			Self::SAR(money) => Self::SAR(money.div_by_non_zero_u64(rhs)),
			Self::SEK(money) => Self::SEK(money.div_by_non_zero_u64(rhs)),
			Self::SGD(money) => Self::SGD(money.div_by_non_zero_u64(rhs)),
			Self::THB(money) => Self::THB(money.div_by_non_zero_u64(rhs)),
			Self::TRY(money) => Self::TRY(money.div_by_non_zero_u64(rhs)),
			Self::TWD(money) => Self::TWD(money.div_by_non_zero_u64(rhs)),
			Self::USD(money) => Self::USD(money.div_by_non_zero_u64(rhs)),
			Self::ZAR(money) => Self::ZAR(money.div_by_non_zero_u64(rhs)),
		}
	}

	/// Takes `dp` and returns [`Moolah`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original amount if `dp` exceeds [`SCALE`](Money::SCALE).
	/// Returns [`Money::MAX`] when rounded money exceeds [`Money::MAX`].
	///
	/// # Rounding
	///
	/// Rounds `nanos` using banker's rounding. For more details see [Money]'s documentation.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 987_654_321)?);
	///
	/// assert_eq!(moolah.saturating_round_nanos(0), Moolah::USD(Money::try_new(43, 0)?));
	/// assert_eq!(moolah.saturating_round_nanos(4), Moolah::USD(Money::try_new(42, 987_700_000)?));
	/// assert_eq!(moolah.saturating_round_nanos(5), Moolah::USD(Money::try_new(42, 987_650_000)?));
	/// assert_eq!(moolah.saturating_round_nanos(10), moolah);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn saturating_round_nanos(&self, dp: u8) -> Self {
		match self {
			Self::AED(money) => Self::AED(money.saturating_round_nanos(dp)),
			Self::ARS(money) => Self::ARS(money.saturating_round_nanos(dp)),
			Self::AUD(money) => Self::AUD(money.saturating_round_nanos(dp)),
			Self::BGN(money) => Self::BGN(money.saturating_round_nanos(dp)),
			Self::BHD(money) => Self::BHD(money.saturating_round_nanos(dp)),
			Self::BOV(money) => Self::BOV(money.saturating_round_nanos(dp)),
			Self::BRL(money) => Self::BRL(money.saturating_round_nanos(dp)),
			Self::CAD(money) => Self::CAD(money.saturating_round_nanos(dp)),
			Self::CHF(money) => Self::CHF(money.saturating_round_nanos(dp)),
			Self::CLP(money) => Self::CLP(money.saturating_round_nanos(dp)),
			Self::CNY(money) => Self::CNY(money.saturating_round_nanos(dp)),
			Self::COP(money) => Self::COP(money.saturating_round_nanos(dp)),
			Self::CZK(money) => Self::CZK(money.saturating_round_nanos(dp)),
			Self::DKK(money) => Self::DKK(money.saturating_round_nanos(dp)),
			Self::EUR(money) => Self::EUR(money.saturating_round_nanos(dp)),
			Self::GBP(money) => Self::GBP(money.saturating_round_nanos(dp)),
			Self::HKD(money) => Self::HKD(money.saturating_round_nanos(dp)),
			Self::HUF(money) => Self::HUF(money.saturating_round_nanos(dp)),
			Self::IDR(money) => Self::IDR(money.saturating_round_nanos(dp)),
			Self::ILS(money) => Self::ILS(money.saturating_round_nanos(dp)),
			Self::INR(money) => Self::INR(money.saturating_round_nanos(dp)),
			Self::JPY(money) => Self::JPY(money.saturating_round_nanos(dp)),
			Self::KRW(money) => Self::KRW(money.saturating_round_nanos(dp)),
			Self::MXN(money) => Self::MXN(money.saturating_round_nanos(dp)),
			Self::MYR(money) => Self::MYR(money.saturating_round_nanos(dp)),
			Self::NOK(money) => Self::NOK(money.saturating_round_nanos(dp)),
			Self::NZD(money) => Self::NZD(money.saturating_round_nanos(dp)),
			Self::PEN(money) => Self::PEN(money.saturating_round_nanos(dp)),
			Self::PHP(money) => Self::PHP(money.saturating_round_nanos(dp)),
			Self::PLN(money) => Self::PLN(money.saturating_round_nanos(dp)),
			Self::RON(money) => Self::RON(money.saturating_round_nanos(dp)),
			Self::RUB(money) => Self::RUB(money.saturating_round_nanos(dp)),
			Self::SAR(money) => Self::SAR(money.saturating_round_nanos(dp)),
			Self::SEK(money) => Self::SEK(money.saturating_round_nanos(dp)),
			Self::SGD(money) => Self::SGD(money.saturating_round_nanos(dp)),
			Self::THB(money) => Self::THB(money.saturating_round_nanos(dp)),
			Self::TRY(money) => Self::TRY(money.saturating_round_nanos(dp)),
			Self::TWD(money) => Self::TWD(money.saturating_round_nanos(dp)),
			Self::USD(money) => Self::USD(money.saturating_round_nanos(dp)),
			Self::ZAR(money) => Self::ZAR(money.saturating_round_nanos(dp)),
		}
	}

	/// Takes `dp` and returns [`Moolah`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original amount if `dp` exceeds [`SCALE`](Money::SCALE).
	/// Returns [`Money::MAX`] when rounded money exceeds [`Money::MAX`].
	///
	/// # Rounding
	///
	/// Rounds `nanos` following [rounding up](https://en.wikipedia.org/wiki/Rounding#Rounding_up).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 987_654_321)?);
	///
	/// assert_eq!(moolah.saturating_round_up_nanos(0), Moolah::USD(Money::try_new(43, 0)?));
	/// assert_eq!(moolah.saturating_round_up_nanos(4), Moolah::USD(Money::try_new(42, 987_700_000)?));
	/// assert_eq!(moolah.saturating_round_up_nanos(5), Moolah::USD(Money::try_new(42, 987_660_000)?));
	/// assert_eq!(moolah.saturating_round_up_nanos(10), moolah);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn saturating_round_up_nanos(&self, dp: u8) -> Self {
		match self {
			Self::AED(money) => Self::AED(money.saturating_round_up_nanos(dp)),
			Self::ARS(money) => Self::ARS(money.saturating_round_up_nanos(dp)),
			Self::AUD(money) => Self::AUD(money.saturating_round_up_nanos(dp)),
			Self::BGN(money) => Self::BGN(money.saturating_round_up_nanos(dp)),
			Self::BHD(money) => Self::BHD(money.saturating_round_up_nanos(dp)),
			Self::BOV(money) => Self::BOV(money.saturating_round_up_nanos(dp)),
			Self::BRL(money) => Self::BRL(money.saturating_round_up_nanos(dp)),
			Self::CAD(money) => Self::CAD(money.saturating_round_up_nanos(dp)),
			Self::CHF(money) => Self::CHF(money.saturating_round_up_nanos(dp)),
			Self::CLP(money) => Self::CLP(money.saturating_round_up_nanos(dp)),
			Self::CNY(money) => Self::CNY(money.saturating_round_up_nanos(dp)),
			Self::COP(money) => Self::COP(money.saturating_round_up_nanos(dp)),
			Self::CZK(money) => Self::CZK(money.saturating_round_up_nanos(dp)),
			Self::DKK(money) => Self::DKK(money.saturating_round_up_nanos(dp)),
			Self::EUR(money) => Self::EUR(money.saturating_round_up_nanos(dp)),
			Self::GBP(money) => Self::GBP(money.saturating_round_up_nanos(dp)),
			Self::HKD(money) => Self::HKD(money.saturating_round_up_nanos(dp)),
			Self::HUF(money) => Self::HUF(money.saturating_round_up_nanos(dp)),
			Self::IDR(money) => Self::IDR(money.saturating_round_up_nanos(dp)),
			Self::ILS(money) => Self::ILS(money.saturating_round_up_nanos(dp)),
			Self::INR(money) => Self::INR(money.saturating_round_up_nanos(dp)),
			Self::JPY(money) => Self::JPY(money.saturating_round_up_nanos(dp)),
			Self::KRW(money) => Self::KRW(money.saturating_round_up_nanos(dp)),
			Self::MXN(money) => Self::MXN(money.saturating_round_up_nanos(dp)),
			Self::MYR(money) => Self::MYR(money.saturating_round_up_nanos(dp)),
			Self::NOK(money) => Self::NOK(money.saturating_round_up_nanos(dp)),
			Self::NZD(money) => Self::NZD(money.saturating_round_up_nanos(dp)),
			Self::PEN(money) => Self::PEN(money.saturating_round_up_nanos(dp)),
			Self::PHP(money) => Self::PHP(money.saturating_round_up_nanos(dp)),
			Self::PLN(money) => Self::PLN(money.saturating_round_up_nanos(dp)),
			Self::RON(money) => Self::RON(money.saturating_round_up_nanos(dp)),
			Self::RUB(money) => Self::RUB(money.saturating_round_up_nanos(dp)),
			Self::SAR(money) => Self::SAR(money.saturating_round_up_nanos(dp)),
			Self::SEK(money) => Self::SEK(money.saturating_round_up_nanos(dp)),
			Self::SGD(money) => Self::SGD(money.saturating_round_up_nanos(dp)),
			Self::THB(money) => Self::THB(money.saturating_round_up_nanos(dp)),
			Self::TRY(money) => Self::TRY(money.saturating_round_up_nanos(dp)),
			Self::TWD(money) => Self::TWD(money.saturating_round_up_nanos(dp)),
			Self::USD(money) => Self::USD(money.saturating_round_up_nanos(dp)),
			Self::ZAR(money) => Self::ZAR(money.saturating_round_up_nanos(dp)),
		}
	}

	/// Takes `dp` and returns [`Moolah`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original amount if `dp` exceeds [`SCALE`](Money::SCALE).
	///
	/// # Rounding
	///
	/// Rounds `nanos` following [rounding-down](https://en.wikipedia.org/wiki/Rounding#Rounding_down).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 123_456_789)?);
	///
	/// assert_eq!(moolah.round_down_nanos(0), Moolah::USD(Money::try_new(42, 0)?));
	/// assert_eq!(moolah.round_down_nanos(3), Moolah::USD(Money::try_new(42, 123_000_000)?));
	/// assert_eq!(moolah.round_down_nanos(5), Moolah::USD(Money::try_new(42, 123_450_000)?));
	/// assert_eq!(moolah.round_down_nanos(10), moolah);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn round_down_nanos(&self, dp: u8) -> Self {
		match self {
			Self::AED(money) => Self::AED(money.round_down_nanos(dp)),
			Self::ARS(money) => Self::ARS(money.round_down_nanos(dp)),
			Self::AUD(money) => Self::AUD(money.round_down_nanos(dp)),
			Self::BGN(money) => Self::BGN(money.round_down_nanos(dp)),
			Self::BHD(money) => Self::BHD(money.round_down_nanos(dp)),
			Self::BOV(money) => Self::BOV(money.round_down_nanos(dp)),
			Self::BRL(money) => Self::BRL(money.round_down_nanos(dp)),
			Self::CAD(money) => Self::CAD(money.round_down_nanos(dp)),
			Self::CHF(money) => Self::CHF(money.round_down_nanos(dp)),
			Self::CLP(money) => Self::CLP(money.round_down_nanos(dp)),
			Self::CNY(money) => Self::CNY(money.round_down_nanos(dp)),
			Self::COP(money) => Self::COP(money.round_down_nanos(dp)),
			Self::CZK(money) => Self::CZK(money.round_down_nanos(dp)),
			Self::DKK(money) => Self::DKK(money.round_down_nanos(dp)),
			Self::EUR(money) => Self::EUR(money.round_down_nanos(dp)),
			Self::GBP(money) => Self::GBP(money.round_down_nanos(dp)),
			Self::HKD(money) => Self::HKD(money.round_down_nanos(dp)),
			Self::HUF(money) => Self::HUF(money.round_down_nanos(dp)),
			Self::IDR(money) => Self::IDR(money.round_down_nanos(dp)),
			Self::ILS(money) => Self::ILS(money.round_down_nanos(dp)),
			Self::INR(money) => Self::INR(money.round_down_nanos(dp)),
			Self::JPY(money) => Self::JPY(money.round_down_nanos(dp)),
			Self::KRW(money) => Self::KRW(money.round_down_nanos(dp)),
			Self::MXN(money) => Self::MXN(money.round_down_nanos(dp)),
			Self::MYR(money) => Self::MYR(money.round_down_nanos(dp)),
			Self::NOK(money) => Self::NOK(money.round_down_nanos(dp)),
			Self::NZD(money) => Self::NZD(money.round_down_nanos(dp)),
			Self::PEN(money) => Self::PEN(money.round_down_nanos(dp)),
			Self::PHP(money) => Self::PHP(money.round_down_nanos(dp)),
			Self::PLN(money) => Self::PLN(money.round_down_nanos(dp)),
			Self::RON(money) => Self::RON(money.round_down_nanos(dp)),
			Self::RUB(money) => Self::RUB(money.round_down_nanos(dp)),
			Self::SAR(money) => Self::SAR(money.round_down_nanos(dp)),
			Self::SEK(money) => Self::SEK(money.round_down_nanos(dp)),
			Self::SGD(money) => Self::SGD(money.round_down_nanos(dp)),
			Self::THB(money) => Self::THB(money.round_down_nanos(dp)),
			Self::TRY(money) => Self::TRY(money.round_down_nanos(dp)),
			Self::TWD(money) => Self::TWD(money.round_down_nanos(dp)),
			Self::USD(money) => Self::USD(money.round_down_nanos(dp)),
			Self::ZAR(money) => Self::ZAR(money.round_down_nanos(dp)),
		}
	}

	/// Returns the amount as [String] in the format `{units}.{nanos}`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 123_456_789)?);
	///
	/// assert_eq!(moolah.to_amount_str(), "42.123456789");
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn to_amount_str(&self) -> String {
		match self {
			Self::AED(money) => money.to_amount_str(),
			Self::ARS(money) => money.to_amount_str(),
			Self::AUD(money) => money.to_amount_str(),
			Self::BGN(money) => money.to_amount_str(),
			Self::BHD(money) => money.to_amount_str(),
			Self::BOV(money) => money.to_amount_str(),
			Self::BRL(money) => money.to_amount_str(),
			Self::CAD(money) => money.to_amount_str(),
			Self::CHF(money) => money.to_amount_str(),
			Self::CLP(money) => money.to_amount_str(),
			Self::CNY(money) => money.to_amount_str(),
			Self::COP(money) => money.to_amount_str(),
			Self::CZK(money) => money.to_amount_str(),
			Self::DKK(money) => money.to_amount_str(),
			Self::EUR(money) => money.to_amount_str(),
			Self::GBP(money) => money.to_amount_str(),
			Self::HKD(money) => money.to_amount_str(),
			Self::HUF(money) => money.to_amount_str(),
			Self::IDR(money) => money.to_amount_str(),
			Self::ILS(money) => money.to_amount_str(),
			Self::INR(money) => money.to_amount_str(),
			Self::JPY(money) => money.to_amount_str(),
			Self::KRW(money) => money.to_amount_str(),
			Self::MXN(money) => money.to_amount_str(),
			Self::MYR(money) => money.to_amount_str(),
			Self::NOK(money) => money.to_amount_str(),
			Self::NZD(money) => money.to_amount_str(),
			Self::PEN(money) => money.to_amount_str(),
			Self::PHP(money) => money.to_amount_str(),
			Self::PLN(money) => money.to_amount_str(),
			Self::RON(money) => money.to_amount_str(),
			Self::RUB(money) => money.to_amount_str(),
			Self::SAR(money) => money.to_amount_str(),
			Self::SEK(money) => money.to_amount_str(),
			Self::SGD(money) => money.to_amount_str(),
			Self::THB(money) => money.to_amount_str(),
			Self::TRY(money) => money.to_amount_str(),
			Self::TWD(money) => money.to_amount_str(),
			Self::USD(money) => money.to_amount_str(),
			Self::ZAR(money) => money.to_amount_str(),
		}
	}

	/// Takes `dp`, and returns the amount as [String] according to the format:
	///
	/// - `{units}` when `dp == 0`.
	///
	/// - `{units}.{truncated_nanos}` otherwise,
	/// `truncated_nanos` represents `dp` most-significant decimal digits of `nanos`.
	///
	/// Returns the same as [Moolah::to_amount_str] when `dp` exceeds [`SCALE`](Money::SCALE).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moolah = Moolah::USD(Money::try_new(42, 123_456_789)?);
	///
	/// assert_eq!(moolah.to_amount_str_dp(0), "42");
	/// assert_eq!(moolah.to_amount_str_dp(5), "42.12345");
	/// assert_eq!(moolah.to_amount_str_dp(u8::MAX), "42.123456789");
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn to_amount_str_dp(&self, dp: u8) -> String {
		match self {
			Self::AED(money) => money.to_amount_str_dp(dp),
			Self::ARS(money) => money.to_amount_str_dp(dp),
			Self::AUD(money) => money.to_amount_str_dp(dp),
			Self::BGN(money) => money.to_amount_str_dp(dp),
			Self::BHD(money) => money.to_amount_str_dp(dp),
			Self::BOV(money) => money.to_amount_str_dp(dp),
			Self::BRL(money) => money.to_amount_str_dp(dp),
			Self::CAD(money) => money.to_amount_str_dp(dp),
			Self::CHF(money) => money.to_amount_str_dp(dp),
			Self::CLP(money) => money.to_amount_str_dp(dp),
			Self::CNY(money) => money.to_amount_str_dp(dp),
			Self::COP(money) => money.to_amount_str_dp(dp),
			Self::CZK(money) => money.to_amount_str_dp(dp),
			Self::DKK(money) => money.to_amount_str_dp(dp),
			Self::EUR(money) => money.to_amount_str_dp(dp),
			Self::GBP(money) => money.to_amount_str_dp(dp),
			Self::HKD(money) => money.to_amount_str_dp(dp),
			Self::HUF(money) => money.to_amount_str_dp(dp),
			Self::IDR(money) => money.to_amount_str_dp(dp),
			Self::ILS(money) => money.to_amount_str_dp(dp),
			Self::INR(money) => money.to_amount_str_dp(dp),
			Self::JPY(money) => money.to_amount_str_dp(dp),
			Self::KRW(money) => money.to_amount_str_dp(dp),
			Self::MXN(money) => money.to_amount_str_dp(dp),
			Self::MYR(money) => money.to_amount_str_dp(dp),
			Self::NOK(money) => money.to_amount_str_dp(dp),
			Self::NZD(money) => money.to_amount_str_dp(dp),
			Self::PEN(money) => money.to_amount_str_dp(dp),
			Self::PHP(money) => money.to_amount_str_dp(dp),
			Self::PLN(money) => money.to_amount_str_dp(dp),
			Self::RON(money) => money.to_amount_str_dp(dp),
			Self::RUB(money) => money.to_amount_str_dp(dp),
			Self::SAR(money) => money.to_amount_str_dp(dp),
			Self::SEK(money) => money.to_amount_str_dp(dp),
			Self::SGD(money) => money.to_amount_str_dp(dp),
			Self::THB(money) => money.to_amount_str_dp(dp),
			Self::TRY(money) => money.to_amount_str_dp(dp),
			Self::TWD(money) => money.to_amount_str_dp(dp),
			Self::USD(money) => money.to_amount_str_dp(dp),
			Self::ZAR(money) => money.to_amount_str_dp(dp),
		}
	}

	/// Compares `self` to `rhs` and returns the [`Ordering`].
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Examples
	/// ```
	/// # use core::cmp::Ordering;
	/// #
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(42, 123_456_789)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 123_456_789)?);
	///
	/// let ordering = lhs.try_cmp(&rhs)?;
	///
	/// assert_eq!(ordering, Ordering::Equal);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use core::cmp::Ordering;
	/// #
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(25, 700)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 123_456_789)?);
	///
	/// let ordering = lhs.try_cmp(&rhs)?;
	///
	/// assert_eq!(ordering, Ordering::Less);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use core::cmp::Ordering;
	/// #
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::ZERO);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// let ordering = lhs.try_cmp(&rhs);
	///
	/// assert_eq!(ordering, None);
	/// ```
	pub fn try_cmp(&self, rhs: &Self) -> Option<Ordering> {
		match (self, rhs) {
			(Moolah::AED(lhs), Moolah::AED(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::ARS(lhs), Moolah::ARS(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::AUD(lhs), Moolah::AUD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::BGN(lhs), Moolah::BGN(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::BHD(lhs), Moolah::BHD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::BOV(lhs), Moolah::BOV(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::BRL(lhs), Moolah::BRL(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::CAD(lhs), Moolah::CAD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::CHF(lhs), Moolah::CHF(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::CLP(lhs), Moolah::CLP(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::CNY(lhs), Moolah::CNY(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::COP(lhs), Moolah::COP(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::CZK(lhs), Moolah::CZK(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::DKK(lhs), Moolah::DKK(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::EUR(lhs), Moolah::EUR(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::GBP(lhs), Moolah::GBP(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::HKD(lhs), Moolah::HKD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::HUF(lhs), Moolah::HUF(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::IDR(lhs), Moolah::IDR(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::ILS(lhs), Moolah::ILS(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::INR(lhs), Moolah::INR(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::JPY(lhs), Moolah::JPY(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::KRW(lhs), Moolah::KRW(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::MXN(lhs), Moolah::MXN(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::MYR(lhs), Moolah::MYR(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::NOK(lhs), Moolah::NOK(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::NZD(lhs), Moolah::NZD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::PEN(lhs), Moolah::PEN(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::PHP(lhs), Moolah::PHP(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::PLN(lhs), Moolah::PLN(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::RON(lhs), Moolah::RON(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::RUB(lhs), Moolah::RUB(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::SAR(lhs), Moolah::SAR(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::SEK(lhs), Moolah::SEK(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::SGD(lhs), Moolah::SGD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::THB(lhs), Moolah::THB(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::TRY(lhs), Moolah::TRY(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::TWD(lhs), Moolah::TWD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::USD(lhs), Moolah::USD(rhs)) => lhs.partial_cmp(rhs),
			(Moolah::ZAR(lhs), Moolah::ZAR(rhs)) => lhs.partial_cmp(rhs),
			_ => None,
		}
	}

	/// Tests whether `self` equals `rhs`.
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Comparison with [PartialEq]
	///
	/// [Moolah] also implements [PartialEq] to facilitate [assert_eq] assertions.
	/// However, this method should be used to check equality because it checks whether
	/// both `self` and `rhs` are the same variant, then compares the amounts.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(42, 100)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 100)?);
	///
	/// assert_eq!(lhs.try_eq(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(24, 100)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 100)?);
	///
	/// assert_eq!(lhs.try_eq(&rhs)?, false);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::MAX);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// assert_eq!(lhs.try_eq(&rhs), None);
	/// ```
	pub fn try_eq(&self, rhs: &Self) -> Option<bool> {
		self.try_cmp(rhs).map(|o| match o {
			Ordering::Equal => true,
			Ordering::Less | Ordering::Greater => false,
		})
	}

	/// Tests whether `rhs` exceeds `self`.
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(29, 786_420)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 100)?);
	///
	/// assert_eq!(lhs.try_lt(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(29, 31)?);
	/// let rhs = Moolah::USD(Money::try_new(23, 19)?);
	///
	/// assert_eq!(lhs.try_lt(&rhs)?, false);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::ZERO);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// assert_eq!(lhs.try_lt(&rhs), None);
	/// ```
	pub fn try_lt(&self, rhs: &Self) -> Option<bool> {
		self.try_cmp(rhs).map(|o| match o {
			Ordering::Less => true,
			Ordering::Equal | Ordering::Greater => false,
		})
	}

	/// Tests whether `rhs` exceeds or equals `self`.
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(29, 786_420)?);
	/// let rhs = Moolah::USD(Money::try_new(42, 100)?);
	///
	/// assert_eq!(lhs.try_le(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(63, 253)?);
	/// let rhs = Moolah::USD(Money::try_new(63, 253)?);
	///
	/// assert_eq!(lhs.try_le(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(29, 31)?);
	/// let rhs = Moolah::USD(Money::try_new(23, 19)?);
	///
	/// assert_eq!(lhs.try_le(&rhs)?, false);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::ZERO);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// assert_eq!(lhs.try_le(&rhs), None);
	/// ```
	pub fn try_le(&self, rhs: &Self) -> Option<bool> {
		self.try_cmp(rhs).map(|o| match o {
			Ordering::Less | Ordering::Equal => true,
			Ordering::Greater => false,
		})
	}

	/// Tests whether `self` exceeds `rhs`.
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(53, 79)?);
	/// let rhs = Moolah::USD(Money::try_new(29, 786_420)?);
	///
	/// assert_eq!(lhs.try_gt(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(17, 121)?);
	/// let rhs = Moolah::USD(Money::try_new(31, 93)?);
	///
	/// assert_eq!(lhs.try_gt(&rhs)?, false);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::ZERO);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// assert_eq!(lhs.try_gt(&rhs), None);
	/// ```
	pub fn try_gt(&self, rhs: &Self) -> Option<bool> {
		self.try_cmp(rhs).map(|o| match o {
			Ordering::Greater => true,
			Ordering::Less | Ordering::Equal => false,
		})
	}

	/// Tests whether `self` exceeds or equals `rhs`.
	/// Returns [None] when `self` and `rhs` are not the same variant.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(53, 79)?);
	/// let rhs = Moolah::USD(Money::try_new(29, 786_420)?);
	///
	/// assert_eq!(lhs.try_ge(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(90, 79)?);
	/// let rhs = Moolah::USD(Money::try_new(90, 79)?);
	///
	/// assert_eq!(lhs.try_ge(&rhs)?, true);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Moolah::USD(Money::try_new(17, 121)?);
	/// let rhs = Moolah::USD(Money::try_new(31, 93)?);
	///
	/// assert_eq!(lhs.try_ge(&rhs)?, false);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{money::Money, Moolah};
	/// #
	/// let lhs = Moolah::USD(Money::ZERO);
	/// let rhs = Moolah::EUR(Money::MAX);
	///
	/// assert_eq!(lhs.try_ge(&rhs), None);
	/// ```
	pub fn try_ge(&self, rhs: &Self) -> Option<bool> {
		self.try_cmp(rhs).map(|o| match o {
			Ordering::Greater | Ordering::Equal => true,
			Ordering::Less => false,
		})
	}
}

impl<C> From<Money<C>> for Moolah
where
	C: Currency + Default,
{
	fn from(money: Money<C>) -> Self {
		Self::from(&money)
	}
}

impl<C> From<&Money<C>> for Moolah
where
	C: Currency + Default,
{
	fn from(money: &Money<C>) -> Self {
		match C::default().mudra() {
			Mudra::AED => Self::AED(money.with_currency::<AED>()),
			Mudra::ARS => Self::ARS(money.with_currency::<ARS>()),
			Mudra::AUD => Self::AUD(money.with_currency::<AUD>()),
			Mudra::BGN => Self::BGN(money.with_currency::<BGN>()),
			Mudra::BHD => Self::BHD(money.with_currency::<BHD>()),
			Mudra::BOV => Self::BOV(money.with_currency::<BOV>()),
			Mudra::BRL => Self::BRL(money.with_currency::<BRL>()),
			Mudra::CAD => Self::CAD(money.with_currency::<CAD>()),
			Mudra::CHF => Self::CHF(money.with_currency::<CHF>()),
			Mudra::CLP => Self::CLP(money.with_currency::<CLP>()),
			Mudra::CNY => Self::CNY(money.with_currency::<CNY>()),
			Mudra::COP => Self::COP(money.with_currency::<COP>()),
			Mudra::CZK => Self::CZK(money.with_currency::<CZK>()),
			Mudra::DKK => Self::DKK(money.with_currency::<DKK>()),
			Mudra::EUR => Self::EUR(money.with_currency::<EUR>()),
			Mudra::GBP => Self::GBP(money.with_currency::<GBP>()),
			Mudra::HKD => Self::HKD(money.with_currency::<HKD>()),
			Mudra::HUF => Self::HUF(money.with_currency::<HUF>()),
			Mudra::IDR => Self::IDR(money.with_currency::<IDR>()),
			Mudra::ILS => Self::ILS(money.with_currency::<ILS>()),
			Mudra::INR => Self::INR(money.with_currency::<INR>()),
			Mudra::JPY => Self::JPY(money.with_currency::<JPY>()),
			Mudra::KRW => Self::KRW(money.with_currency::<KRW>()),
			Mudra::MXN => Self::MXN(money.with_currency::<MXN>()),
			Mudra::MYR => Self::MYR(money.with_currency::<MYR>()),
			Mudra::NOK => Self::NOK(money.with_currency::<NOK>()),
			Mudra::NZD => Self::NZD(money.with_currency::<NZD>()),
			Mudra::PEN => Self::PEN(money.with_currency::<PEN>()),
			Mudra::PHP => Self::PHP(money.with_currency::<PHP>()),
			Mudra::PLN => Self::PLN(money.with_currency::<PLN>()),
			Mudra::RON => Self::RON(money.with_currency::<RON>()),
			Mudra::RUB => Self::RUB(money.with_currency::<RUB>()),
			Mudra::SAR => Self::SAR(money.with_currency::<SAR>()),
			Mudra::SEK => Self::SEK(money.with_currency::<SEK>()),
			Mudra::SGD => Self::SGD(money.with_currency::<SGD>()),
			Mudra::THB => Self::THB(money.with_currency::<THB>()),
			Mudra::TRY => Self::TRY(money.with_currency::<TRY>()),
			Mudra::TWD => Self::TWD(money.with_currency::<TWD>()),
			Mudra::USD => Self::USD(money.with_currency::<USD>()),
			Mudra::ZAR => Self::ZAR(money.with_currency::<ZAR>()),
		}
	}
}

impl<M> TryFrom<(M, Decimal)> for Moolah
where
	M: Borrow<Mudra>,
{
	type Error = ();

	fn try_from((mudra, amount): (M, Decimal)) -> Result<Self, Self::Error> {
		let moolah = match mudra.borrow() {
			Mudra::AED => Self::AED(amount.try_into()?),
			Mudra::ARS => Self::ARS(amount.try_into()?),
			Mudra::AUD => Self::AUD(amount.try_into()?),
			Mudra::BGN => Self::BGN(amount.try_into()?),
			Mudra::BHD => Self::BHD(amount.try_into()?),
			Mudra::BOV => Self::BOV(amount.try_into()?),
			Mudra::BRL => Self::BRL(amount.try_into()?),
			Mudra::CAD => Self::CAD(amount.try_into()?),
			Mudra::CHF => Self::CHF(amount.try_into()?),
			Mudra::CLP => Self::CLP(amount.try_into()?),
			Mudra::CNY => Self::CNY(amount.try_into()?),
			Mudra::COP => Self::COP(amount.try_into()?),
			Mudra::CZK => Self::CZK(amount.try_into()?),
			Mudra::DKK => Self::DKK(amount.try_into()?),
			Mudra::EUR => Self::EUR(amount.try_into()?),
			Mudra::GBP => Self::GBP(amount.try_into()?),
			Mudra::HKD => Self::HKD(amount.try_into()?),
			Mudra::HUF => Self::HUF(amount.try_into()?),
			Mudra::IDR => Self::IDR(amount.try_into()?),
			Mudra::ILS => Self::ILS(amount.try_into()?),
			Mudra::INR => Self::INR(amount.try_into()?),
			Mudra::JPY => Self::JPY(amount.try_into()?),
			Mudra::KRW => Self::KRW(amount.try_into()?),
			Mudra::MXN => Self::MXN(amount.try_into()?),
			Mudra::MYR => Self::MYR(amount.try_into()?),
			Mudra::NOK => Self::NOK(amount.try_into()?),
			Mudra::NZD => Self::NZD(amount.try_into()?),
			Mudra::PEN => Self::PEN(amount.try_into()?),
			Mudra::PHP => Self::PHP(amount.try_into()?),
			Mudra::PLN => Self::PLN(amount.try_into()?),
			Mudra::RON => Self::RON(amount.try_into()?),
			Mudra::RUB => Self::RUB(amount.try_into()?),
			Mudra::SAR => Self::SAR(amount.try_into()?),
			Mudra::SEK => Self::SEK(amount.try_into()?),
			Mudra::SGD => Self::SGD(amount.try_into()?),
			Mudra::THB => Self::THB(amount.try_into()?),
			Mudra::TRY => Self::TRY(amount.try_into()?),
			Mudra::TWD => Self::TWD(amount.try_into()?),
			Mudra::USD => Self::USD(amount.try_into()?),
			Mudra::ZAR => Self::ZAR(amount.try_into()?),
		};

		Ok(moolah)
	}
}

impl From<Moolah> for Decimal {
	fn from(moolah: Moolah) -> Self {
		Self::from(&moolah)
	}
}

impl From<&Moolah> for Decimal {
	fn from(moolah: &Moolah) -> Self {
		match moolah {
			Moolah::AED(money) => money.into(),
			Moolah::ARS(money) => money.into(),
			Moolah::AUD(money) => money.into(),
			Moolah::BGN(money) => money.into(),
			Moolah::BHD(money) => money.into(),
			Moolah::BOV(money) => money.into(),
			Moolah::BRL(money) => money.into(),
			Moolah::CAD(money) => money.into(),
			Moolah::CHF(money) => money.into(),
			Moolah::CLP(money) => money.into(),
			Moolah::CNY(money) => money.into(),
			Moolah::COP(money) => money.into(),
			Moolah::CZK(money) => money.into(),
			Moolah::DKK(money) => money.into(),
			Moolah::EUR(money) => money.into(),
			Moolah::GBP(money) => money.into(),
			Moolah::HKD(money) => money.into(),
			Moolah::HUF(money) => money.into(),
			Moolah::IDR(money) => money.into(),
			Moolah::ILS(money) => money.into(),
			Moolah::INR(money) => money.into(),
			Moolah::JPY(money) => money.into(),
			Moolah::KRW(money) => money.into(),
			Moolah::MXN(money) => money.into(),
			Moolah::MYR(money) => money.into(),
			Moolah::NOK(money) => money.into(),
			Moolah::NZD(money) => money.into(),
			Moolah::PEN(money) => money.into(),
			Moolah::PHP(money) => money.into(),
			Moolah::PLN(money) => money.into(),
			Moolah::RON(money) => money.into(),
			Moolah::RUB(money) => money.into(),
			Moolah::SAR(money) => money.into(),
			Moolah::SEK(money) => money.into(),
			Moolah::SGD(money) => money.into(),
			Moolah::THB(money) => money.into(),
			Moolah::TRY(money) => money.into(),
			Moolah::TWD(money) => money.into(),
			Moolah::USD(money) => money.into(),
			Moolah::ZAR(money) => money.into(),
		}
	}
}
