use crate::Mudra;

#[derive(Debug, thiserror::Error, PartialEq, Eq)]
#[error("currency mismatch: {} vs {}", .0.code(), .1.code())]
pub struct MismatchError(Mudra, Mudra);

impl MismatchError {
	pub fn pair(&self) -> (Mudra, Mudra) {
		(self.0, self.1)
	}

	pub(crate) fn new(first: Mudra, second: Mudra) -> Self {
		Self(first, second)
	}
}
