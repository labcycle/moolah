//! Module to house [Money] struct.

use core::{borrow::Borrow, marker::PhantomData, num::NonZeroU64};

use rust_decimal::{prelude::ToPrimitive, Decimal, RoundingStrategy};

use crate::{Currency, MismatchError, Moolah, Mudra};

/// Represents an amount of a given currency `C`.
///
/// A [`Money`] represents a financial amount by using the concept of decimal `units` and
/// `nano subunits` (referred to as `nanos` hereon) related by `1 unit = 1_000_000_000 nanos`.
///
/// An amount is comprised of
/// `units` in the range [[`MIN_UNITS`](Self::MIN_UNITS), [`MAX_UNITS`](Self::MAX_UNITS)],
/// and `nanos` in the range [[`MIN_NANOS`](Self::MIN_NANOS), [`MAX_NANOS`](Self::MAX_NANOS)].
///
/// ### Rounding
///
/// If an operation (e.g. [Money::saturating_round_nanos]) needs to round `nanos`,
/// unless specified otherwise, it will [round half to even], also known as banker's rounding.
///
/// [round half to even]: https://en.wikipedia.org/wiki/Rounding#Rounding_half_to_even
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Money<C> {
	amount: Decimal,
	_phantom: PhantomData<C>,
}

impl<C> Money<C> {
	/// [`Money`] with 0 units and 0 nanos.
	/// It also has the smallest possible amount representable by [Money].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// let zero_money = Money::<USD>::ZERO;
	///
	/// assert_eq!(zero_money.units(), 0);
	/// assert_eq!(zero_money.nanos(), 0);
	/// ```
	pub const ZERO: Self = {
		let amount = Decimal::from_parts(0, 0, 0, false, Self::SCALE as u32);
		Self::with_sanitized_decimal_amount(amount)
	};

	/// [`Money`] with [MAX_UNITS](Self::MAX_UNITS) units and [MAX_NANOS](Self::MAX_NANOS) nanos.
	/// It also has the largest possible amount representable by [Money].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// let max_money = Money::<USD>::MAX;
	///
	/// assert_eq!(max_money.units(), u64::MAX);
	/// assert_eq!(max_money.nanos(), 999_999_999);
	/// ```
	pub const MAX: Self = {
		let amount = Decimal::from_parts(
			u32::MAX,
			u32::MAX,
			Self::MAX_NANOS,
			false,
			Self::SCALE as u32,
		);
		Self::with_sanitized_decimal_amount(amount)
	};

	/// Maximum units supported by [Money].
	pub const MAX_UNITS: u64 = u64::MAX;

	/// Maximum nanos supported by [Money].
	pub const MAX_NANOS: u32 = 999_999_999;

	/// Minimum units supported by [Money].
	pub const MIN_UNITS: u64 = 0;

	/// Minimum nanos supported by [Money].
	pub const MIN_NANOS: u32 = 0;

	/// Defines the number of digits after decimal point in decimal representation of amount
	/// i.e. number of digits in [MAX_NANOS](Self::MAX_NANOS).
	pub const SCALE: u8 = 9;

	const DEFAULT_ROUNDING_STRATEGY: RoundingStrategy = RoundingStrategy::MidpointNearestEven;

	/// Creates [`Money`] from `units` and `nanos`.
	/// Returns [None] when `nanos` exceeds [MAX_NANOS](Self::MAX_NANOS).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let valid_money = Money::<USD>::try_new(100, 999_999_999)?;
	///
	/// assert_eq!(valid_money.units(), 100);
	/// assert_eq!(valid_money.nanos(), 999_999_999);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// let invalid_money = Money::<USD>::try_new(100, 1_000_000_000);
	///
	/// assert_eq!(invalid_money, None);
	/// ```
	pub fn try_new(units: u64, nanos: u32) -> Option<Self> {
		if nanos > Self::MAX_NANOS {
			return None;
		}

		let units_decimal = Decimal::from(units);
		let nanos_decimal = Decimal::new(nanos.into(), Self::SCALE.into());
		let amount = units_decimal + nanos_decimal;

		Some(Self::with_sanitized_decimal_amount(amount))
	}

	/// Takes `moneys` i.e. iterator of [Money], and sums up `moneys` to create [`Money`].
	/// Returns [`Money::ZERO`] when `moneys` is empty.
	/// Returns [None] when the sum exceeds [`Money::MAX`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moneys = [
	///     Money::try_new(100, 50)?,
	///     Money::try_new(200, 500)?,
	///     Money::try_new(1000, 5000)?,
	/// ];
	///
	/// let sum = Money::<USD>::try_add_all(moneys)?;
	///
	/// assert_eq!(sum, Money::try_new(1300, 5550)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let moneys = [Money::MAX, Money::try_new(0, 1)?];
	///
	/// let sum = Money::<USD>::try_add_all(moneys);
	///
	/// assert_eq!(sum, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn try_add_all<I>(moneys: I) -> Option<Self>
	where
		I: IntoIterator,
		<I as IntoIterator>::Item: Borrow<Self>,
	{
		moneys
			.into_iter()
			.map(|m| m.borrow().amount)
			.try_fold(Decimal::ZERO, |acc, a| acc.checked_add(a))
			.map(Self::from_unsanitized_decimal_amount)?
	}

	/// Changes the associated currency. The returned [`Money`] references the same amount.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::{EUR, JPY, USD}, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let usd_money = Money::<USD>::try_new(42, 1000)?;
	///
	/// let eur_money: Money<EUR> = usd_money.with_currency();
	///
	/// assert_eq!(eur_money.units(), usd_money.units());
	/// assert_eq!(eur_money.nanos(), usd_money.nanos());
	///
	/// let jpy_money = eur_money.with_currency::<JPY>();
	///
	/// assert_eq!(jpy_money.units(), eur_money.units());
	/// assert_eq!(jpy_money.nanos(), eur_money.nanos());
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn with_currency<C2>(&self) -> Money<C2> {
		Money {
			amount: self.amount,
			_phantom: PhantomData,
		}
	}

	/// Returns `self`'s generic parameter `C` that implements [`Currency`] and [`Default`] traits.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 0)?;
	///
	/// assert_eq!(money.currency(), USD);
	/// #
	/// # Some(())
	/// # }
	/// ```
	#[cfg(not(tarpaulin_include))]
	pub fn currency(&self) -> C
	where
		C: Currency + Default,
	{
		C::default()
	}

	/// Returns `units` of the amount in `self`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 99)?;
	///
	/// assert_eq!(money.units(), 42);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn units(&self) -> u64 {
		self.amount.to_u64().expect("amount must have valid units")
	}

	/// Returns `nanos` of the amount in `self`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 99)?;
	///
	/// assert_eq!(money.nanos(), 99);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn nanos(&self) -> u32 {
		self.amount
			.fract()
			.mantissa()
			.try_into()
			.expect("amount must have valid nanos")
	}

	/// Adds `rhs` to `self`, and returns the sum.
	/// Returns [None] when the sum exceeds [`Money::MAX`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Money::try_new(42, 0)?;
	/// let rhs = Money::try_new(50, 5000)?;
	///
	/// let sum = lhs.checked_add(&rhs)?;
	///
	/// assert_eq!(sum, Money::<USD>::try_new(92, 5000)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Money::<USD>::MAX;
	/// let rhs = Money::try_new(1, 0)?;
	///
	/// let sum = lhs.checked_add(&rhs);
	///
	/// assert_eq!(sum, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn checked_add(&self, rhs: &Self) -> Option<Self> {
		Self::from_unsanitized_decimal_amount(self.amount + rhs.amount)
	}

	/// Takes `moneys` i.e. iterator of [Money], and returns the sum by adding `moneys` to `self`.
	/// Returns original money when `moneys` is empty.
	/// Returns [None] when the sum exceeds [`Money::MAX`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(700, 4450)?;
	/// let moneys = [
	///     Money::try_new(100, 50)?,
	///     Money::try_new(200, 500)?,
	///     Money::try_new(1000, 5000)?,
	/// ];
	///
	/// let sum = money.checked_add_all(moneys)?;
	///
	/// assert_eq!(sum, Money::try_new(2000, 10000)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(10, 0)?;
	/// let moneys = [Money::MAX];
	///
	/// let sum = money.checked_add_all(moneys);
	///
	/// assert_eq!(sum, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn checked_add_all<I>(&self, moneys: I) -> Option<Self>
	where
		I: IntoIterator,
		<I as IntoIterator>::Item: Borrow<Self>,
	{
		self.checked_add(&Self::try_add_all(moneys)?)
	}

	/// Subtracts `rhs` from `self`, and returns the difference.
	/// Returns [None] when `rhs` exceeds `self`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Money::try_new(50, 5000)?;
	/// let rhs = Money::try_new(42, 0)?;
	///
	/// let difference: Money<USD> = lhs.checked_sub(&rhs)?;
	///
	/// assert_eq!(difference, Money::try_new(8, 5000)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let lhs = Money::try_new(1, 0)?;
	/// let rhs = Money::try_new(1, 1)?;
	///
	/// let difference: Option<Money<USD>> = lhs.checked_sub(&rhs);
	///
	/// assert_eq!(difference, None);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn checked_sub(&self, rhs: &Self) -> Option<Self> {
		if self.amount < rhs.amount {
			return None;
		}

		let diff_amount = self.amount - rhs.amount;

		Some(Self::with_sanitized_decimal_amount(diff_amount))
	}

	/// Multiplies `self` by `rhs`, and returns the product.
	/// Returns [None] when the product exceeds [`Money::MAX`].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(50, 550_000_000)?;
	///
	/// assert_eq!(money.checked_mul_by_u64(2)?, Money::try_new(101, 100_000_000)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// let money = Money::<USD>::MAX;
	///
	/// assert_eq!(money.checked_mul_by_u64(2), None);
	/// ```
	pub fn checked_mul_by_u64(&self, rhs: u64) -> Option<Self> {
		self.amount
			.checked_mul(rhs.into())
			.map(Self::from_unsanitized_decimal_amount)?
	}

	/// Divides `self` by `rhs`, and returns the quotient.
	///
	/// # Rounding
	///
	/// Rounds `nanos` using banker's rounding. For more details see [Money]'s documentation.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(6, 3000)?;
	/// let rhs = 2.try_into().ok()?;
	///
	/// assert_eq!(money.div_by_non_zero_u64(rhs), Money::try_new(3, 1500)?);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn div_by_non_zero_u64(&self, rhs: NonZeroU64) -> Self {
		Self::from_unsanitized_decimal_amount(self.amount / Decimal::from(u64::from(rhs)))
			.expect("money division by u64 is infalliable")
	}

	/// Takes `dp` and returns [`Money`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original money if `dp` exceeds [`SCALE`](Self::SCALE).
	/// Returns [`Money::MAX`] when rounded money exceeds [`Money::MAX`].
	///
	/// # Rounding
	///
	/// Rounds `nanos` using banker's rounding. For more details see [Money]'s documentation.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 987_654_321)?;
	///
	/// assert_eq!(money.saturating_round_nanos(0), Money::try_new(43, 0)?);
	/// assert_eq!(money.saturating_round_nanos(4), Money::try_new(42, 987_700_000)?);
	/// assert_eq!(money.saturating_round_nanos(5), Money::try_new(42, 987_650_000)?);
	/// assert_eq!(money.saturating_round_nanos(10), money);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(u64::MAX, 999_999_999)?;
	///
	/// assert_eq!(money.saturating_round_nanos(0), Money::MAX);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn saturating_round_nanos(&self, dp: u8) -> Self {
		if dp >= Self::SCALE {
			return Self { ..*self };
		};

		let mut rounded_amount = self
			.amount
			.round_dp_with_strategy(dp.into(), Self::DEFAULT_ROUNDING_STRATEGY);

		if rounded_amount > Self::MAX.amount {
			Self::MAX
		} else {
			rounded_amount.rescale(Self::SCALE.into());
			Self::with_sanitized_decimal_amount(rounded_amount)
		}
	}

	/// Takes `dp` and returns [`Money`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original money if `dp` exceeds [`SCALE`](Self::SCALE).
	/// Returns [`Money::MAX`] when rounded money exceeds [`Money::MAX`].
	///
	/// # Rounding
	///
	/// Rounds `nanos` following [rounding up](https://en.wikipedia.org/wiki/Rounding#Rounding_up).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 987_654_321)?;
	///
	/// assert_eq!(money.saturating_round_up_nanos(0), Money::try_new(43, 0)?);
	/// assert_eq!(money.saturating_round_up_nanos(4), Money::try_new(42, 987_700_000)?);
	/// assert_eq!(money.saturating_round_up_nanos(5), Money::try_new(42, 987_660_000)?);
	/// assert_eq!(money.saturating_round_up_nanos(10), money);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(u64::MAX, 999_999_999)?;
	///
	/// assert_eq!(money.saturating_round_up_nanos(9), Money::MAX);
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn saturating_round_up_nanos(&self, dp: u8) -> Self {
		if dp >= Self::SCALE {
			return Self { ..*self };
		}

		let mut rounded_amount = self
			.amount
			.round_dp_with_strategy(dp.into(), RoundingStrategy::AwayFromZero);

		if rounded_amount >= Self::MAX.amount {
			Self::MAX
		} else {
			rounded_amount.rescale(Self::SCALE.into());
			Self::with_sanitized_decimal_amount(rounded_amount)
		}
	}

	/// Takes `dp` and returns [`Money`] after rounding to `dp` most-significant
	/// decimal digits of `nanos`. Returns original amount if `dp` exceeds [`SCALE`](Self::SCALE).
	///
	/// # Rounding
	///
	/// Rounds `nanos` following [rounding down].
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 123_456_789)?;
	///
	/// assert_eq!(money.round_down_nanos(0), Money::try_new(42, 0)?);
	/// assert_eq!(money.round_down_nanos(3), Money::try_new(42, 123_000_000)?);
	/// assert_eq!(money.round_down_nanos(5), Money::try_new(42, 123_450_000)?);
	/// assert_eq!(money.round_down_nanos(10), money);
	/// #
	/// # Some(())
	/// # }
	/// ```
	///
	/// [rounding down]: https://en.wikipedia.org/wiki/Rounding#Rounding_down
	pub fn round_down_nanos(&self, dp: u8) -> Self {
		if dp >= Self::SCALE {
			return Self { ..*self };
		}

		let mut rounded_down_amount = self
			.amount
			.round_dp_with_strategy(dp.into(), RoundingStrategy::ToZero);

		rounded_down_amount.rescale(Self::SCALE.into());

		Self::with_sanitized_decimal_amount(rounded_down_amount)
	}

	/// Returns the amount as [String] in the format `{units}.{nanos}`.
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 123_456_789)?;
	///
	/// assert_eq!(money.to_amount_str(), "42.123456789");
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn to_amount_str(&self) -> String {
		self.amount.to_string()
	}

	/// Takes `dp`, and returns the amount as [String] according to the format:
	///
	/// - `{units}` when `dp == 0`.
	///
	/// - `{units}.{truncated_nanos}` otherwise,
	/// `truncated_nanos` represents `dp` most-significant decimal digits of `nanos`.
	///
	/// Returns the same as [Money::to_amount_str] when `dp` exceeds [`SCALE`](Self::SCALE).
	///
	/// # Examples
	///
	/// ```
	/// # use moolah::{currency::USD, money::Money};
	/// #
	/// # fn main() {
	/// #     example().unwrap();
	/// # }
	/// #
	/// # fn example() -> Option<()> {
	/// #
	/// let money = Money::<USD>::try_new(42, 123_456_789)?;
	///
	/// assert_eq!(money.to_amount_str_dp(0), "42");
	/// assert_eq!(money.to_amount_str_dp(5), "42.12345");
	/// assert_eq!(money.to_amount_str_dp(u8::MAX), "42.123456789");
	/// #
	/// # Some(())
	/// # }
	/// ```
	pub fn to_amount_str_dp(&self, dp: u8) -> String {
		if dp >= Self::SCALE {
			return self.to_amount_str();
		};

		let trunc_amount = self.amount.trunc_with_scale(dp.into());
		trunc_amount.to_string()
	}

	fn from_unsanitized_decimal_amount(amount: Decimal) -> Option<Self> {
		if amount.floor().to_u64().is_some() {
			let rounded_amount =
				amount.round_dp_with_strategy(Self::SCALE.into(), Self::DEFAULT_ROUNDING_STRATEGY);
			Some(Self::with_sanitized_decimal_amount(rounded_amount))
		} else {
			None
		}
	}

	const fn with_sanitized_decimal_amount(amount: Decimal) -> Self {
		Self {
			amount,
			_phantom: PhantomData,
		}
	}
}

impl<C> TryFrom<Moolah> for Money<C>
where
	C: Currency + Default,
{
	type Error = MismatchError;

	fn try_from(moolah: Moolah) -> Result<Self, Self::Error> {
		let money = match (C::default().mudra(), moolah) {
			(Mudra::AED, Moolah::AED(money)) => money.with_currency(),
			(Mudra::ARS, Moolah::ARS(money)) => money.with_currency(),
			(Mudra::AUD, Moolah::AUD(money)) => money.with_currency(),
			(Mudra::BGN, Moolah::BGN(money)) => money.with_currency(),
			(Mudra::BHD, Moolah::BHD(money)) => money.with_currency(),
			(Mudra::BOV, Moolah::BOV(money)) => money.with_currency(),
			(Mudra::BRL, Moolah::BRL(money)) => money.with_currency(),
			(Mudra::CAD, Moolah::CAD(money)) => money.with_currency(),
			(Mudra::CHF, Moolah::CHF(money)) => money.with_currency(),
			(Mudra::CLP, Moolah::CLP(money)) => money.with_currency(),
			(Mudra::CNY, Moolah::CNY(money)) => money.with_currency(),
			(Mudra::COP, Moolah::COP(money)) => money.with_currency(),
			(Mudra::CZK, Moolah::CZK(money)) => money.with_currency(),
			(Mudra::DKK, Moolah::DKK(money)) => money.with_currency(),
			(Mudra::EUR, Moolah::EUR(money)) => money.with_currency(),
			(Mudra::GBP, Moolah::GBP(money)) => money.with_currency(),
			(Mudra::HKD, Moolah::HKD(money)) => money.with_currency(),
			(Mudra::HUF, Moolah::HUF(money)) => money.with_currency(),
			(Mudra::IDR, Moolah::IDR(money)) => money.with_currency(),
			(Mudra::ILS, Moolah::ILS(money)) => money.with_currency(),
			(Mudra::INR, Moolah::INR(money)) => money.with_currency(),
			(Mudra::JPY, Moolah::JPY(money)) => money.with_currency(),
			(Mudra::KRW, Moolah::KRW(money)) => money.with_currency(),
			(Mudra::MXN, Moolah::MXN(money)) => money.with_currency(),
			(Mudra::MYR, Moolah::MYR(money)) => money.with_currency(),
			(Mudra::NOK, Moolah::NOK(money)) => money.with_currency(),
			(Mudra::NZD, Moolah::NZD(money)) => money.with_currency(),
			(Mudra::PEN, Moolah::PEN(money)) => money.with_currency(),
			(Mudra::PHP, Moolah::PHP(money)) => money.with_currency(),
			(Mudra::PLN, Moolah::PLN(money)) => money.with_currency(),
			(Mudra::RON, Moolah::RON(money)) => money.with_currency(),
			(Mudra::RUB, Moolah::RUB(money)) => money.with_currency(),
			(Mudra::SAR, Moolah::SAR(money)) => money.with_currency(),
			(Mudra::SEK, Moolah::SEK(money)) => money.with_currency(),
			(Mudra::SGD, Moolah::SGD(money)) => money.with_currency(),
			(Mudra::THB, Moolah::THB(money)) => money.with_currency(),
			(Mudra::TRY, Moolah::TRY(money)) => money.with_currency(),
			(Mudra::TWD, Moolah::TWD(money)) => money.with_currency(),
			(Mudra::USD, Moolah::USD(money)) => money.with_currency(),
			(Mudra::ZAR, Moolah::ZAR(money)) => money.with_currency(),
			(mudra, moolah) => return Err(MismatchError::new(mudra, moolah.mudra())),
		};

		Ok(money)
	}
}

impl<C> TryFrom<Decimal> for Money<C> {
	type Error = ();

	fn try_from(amount: Decimal) -> Result<Self, Self::Error> {
		Self::from_unsanitized_decimal_amount(amount).ok_or(())
	}
}

impl<C> From<Money<C>> for Decimal {
	fn from(money: Money<C>) -> Self {
		Self::from(&money)
	}
}

impl<C> From<&Money<C>> for Decimal {
	fn from(money: &Money<C>) -> Self {
		money.amount
	}
}

#[cfg(test)]
mod tests {
	use std::{iter, num::NonZeroU64};

	use rstest::rstest;
	use rust_decimal::Decimal;

	use crate::currency::{EUR, USD};

	use super::Money;

	#[test]
	fn zero_money_has_zero_units_and_zero_nanos() {
		// Act
		let zero_money = Money::<USD>::ZERO;

		// Assert
		assert_eq!(zero_money.units(), 0);
		assert_eq!(zero_money.nanos(), 0);
	}

	#[test]
	fn max_money_has_u64max_units_and_999999999_nanos() {
		// Act
		let max_money = Money::<USD>::MAX;

		// Assert
		assert_eq!(max_money.units(), u64::MAX);
		assert_eq!(max_money.nanos(), 999_999_999);
	}

	#[test]
	fn try_new_returns_money_when_nanos_do_not_exceed_999999999() {
		// Arrange
		let units = u64::MAX;
		let nanos = 999_999_999;

		// Act
		let money = Money::<USD>::try_new(units, nanos).unwrap();

		// Assert
		assert_eq!(money.units(), units);
		assert_eq!(money.nanos(), nanos);
	}

	#[test]
	fn try_new_returns_none_when_nanos_exceed_999999999() {
		// Arrange
		let units = 42;
		let nanos = 1_000_000_000;

		// Act
		let money = Money::<USD>::try_new(units, nanos);

		// Assert
		assert_eq!(money, None);
	}

	#[test]
	fn try_add_all_returns_zero_money_when_empty_moneys() {
		// Act
		let sum = Money::try_add_all(iter::empty::<Money<USD>>()).unwrap();

		// Assert
		assert_eq!(sum, Money::ZERO);
	}

	#[test]
	fn try_add_all_returns_sum_when_non_empty_moneys_and_sum_does_not_exceed_max_money() {
		// Arrange
		let moneys = [
			Money::try_new(u64::MAX - 2, 0).unwrap(),
			Money::try_new(1, 800_000_000).unwrap(),
			Money::try_new(1, 199_999_999).unwrap(),
		];

		// Act
		let sum = Money::try_add_all(moneys).unwrap();

		// Assert
		let expected_sum = Money::<USD>::try_new(u64::MAX, 999_999_999).unwrap();

		assert_eq!(sum, expected_sum);
	}

	#[test]
	fn try_add_all_returns_none_when_sum_exceeds_max_money() {
		// Arrange
		let moneys = [
			Money::try_new(u64::MAX - 2, 0).unwrap(),
			Money::try_new(1, 800_000_000).unwrap(),
			Money::try_new(1, 200_000_000).unwrap(),
		];

		// Act
		let sum = Money::<USD>::try_add_all(moneys);

		// Assert
		assert_eq!(sum, None);
	}

	#[test]
	fn with_currency_returns_money_with_new_currency_and_original_units_and_original_nanos() {
		// Arrange
		let usd_money = Money::<USD>::try_new(42, 987_654_321).unwrap();

		// Act
		let eur_money: Money<EUR> = usd_money.with_currency();

		// Assert
		assert_eq!(eur_money.units(), usd_money.units());
		assert_eq!(eur_money.nanos(), usd_money.nanos());
	}

	#[test]
	fn adding_with_zero_money_returns_the_original_money() {
		// Arrange
		let money = Money::<USD>::try_new(1, 0).unwrap();

		// Act
		let sum = money.checked_add(&Money::ZERO).unwrap();

		// Assert
		assert_eq!(money, sum);
	}

	#[test]
	fn subtracting_with_zero_money_returns_the_original_money() {
		// Arrange
		let money = Money::<USD>::try_new(1, 0).unwrap();

		// Act
		let difference = money.checked_sub(&Money::ZERO).unwrap();

		// Assert
		assert_eq!(money, difference);
	}

	#[test]
	fn checked_add_returns_sum_when_sum_does_not_exceed_max_money() {
		// Arrange
		let lhs = Money::try_new(u64::MAX - 1, 9).unwrap();
		let rhs = Money::try_new(1, 999_999_990).unwrap();

		// Act
		let sum = lhs.checked_add(&rhs).unwrap();

		// Assert
		let expected_sum = Money::<USD>::try_new(u64::MAX, 999_999_999).unwrap();

		assert_eq!(sum, expected_sum);
	}

	#[test]
	fn checked_add_returns_none_when_sum_exceeds_max_money() {
		// Arrange
		let lhs = Money::<USD>::try_new(u64::MAX, 1).unwrap();
		let rhs = Money::<USD>::try_new(0, 999_999_999).unwrap();

		// Act
		let sum = lhs.checked_add(&rhs);

		// Assert
		assert_eq!(sum, None);
	}

	#[test]
	fn checked_add_all_returns_original_money_when_empty_moneys() {
		// Arrange
		let money = Money::try_new(17, 500).unwrap();

		// Act
		let sum = money.checked_add_all(iter::empty::<Money<USD>>()).unwrap();

		// Assert
		assert_eq!(sum, money);
	}

	#[test]
	fn checked_add_all_returns_sum_when_non_empty_moneys_sum_does_not_exceed_max_money() {
		// Arrange
		let money = Money::try_new(u64::MAX - 2, 0).unwrap();
		let moneys = [
			Money::try_new(1, 800_000_000).unwrap(),
			Money::try_new(1, 199_999_999).unwrap(),
		];

		// Act
		let sum = money.checked_add_all(moneys).unwrap();

		// Assert
		let expected_sum = Money::<USD>::try_new(u64::MAX, 999_999_999).unwrap();

		assert_eq!(sum, expected_sum);
	}

	#[test]
	fn checked_add_all_returns_none_when_sum_exceeds_max_money() {
		// Arrange
		let money = Money::<USD>::try_new(u64::MAX - 2, 0).unwrap();
		let moneys = [
			Money::try_new(1, 800_000_000).unwrap(),
			Money::try_new(1, 200_000_000).unwrap(),
		];

		// Act
		let sum = money.checked_add_all(moneys);

		// Assert
		assert_eq!(sum, None);
	}

	#[test]
	fn checked_sub_returns_difference_when_rhs_does_not_exceed_lhs() {
		// Arrange
		let lhs = Money::try_new(1, 10).unwrap();
		let rhs = Money::try_new(0, 999_999_990).unwrap();

		// Act
		let difference = lhs.checked_sub(&rhs).unwrap();

		// Assert
		let expected_difference = Money::<USD>::try_new(0, 20).unwrap();

		assert_eq!(difference, expected_difference);
	}

	#[test]
	fn checked_sub_returns_none_when_rhs_exceeds_lhs() {
		// Arrange
		let lhs = Money::<USD>::try_new(0, 999_999_990).unwrap();
		let rhs = Money::<USD>::try_new(1, 10).unwrap();

		// Act
		let difference = lhs.checked_sub(&rhs);

		// Assert
		assert_eq!(difference, None);
	}

	#[test]
	fn checked_mul_by_u64_returns_product_when_product_does_not_exceed_max_money() {
		// Arrange
		let money = Money::try_new(u64::MAX / 3, 333_333_333).unwrap();
		let factor = 3;

		// Act
		let product = money.checked_mul_by_u64(factor).unwrap();

		// Assert
		let expected_product = Money::<USD>::MAX;

		assert_eq!(product, expected_product);
	}

	#[rstest]
	#[case(Money::try_new(u64::MAX / 3, 333_333_334).unwrap(), 3)]
	#[case(Money::MAX, u64::MAX)]
	fn checked_mul_by_u64_returns_none_when_product_exceeds_max_money(
		#[case] money: Money<USD>,
		#[case] factor: u64,
	) {
		// Act
		let product = money.checked_mul_by_u64(factor);

		// Assert
		assert_eq!(product, None);
	}

	#[rstest]
	#[case::with_no_rounding_when_fractional_digits_does_not_exceed_9(
		((u64::MAX, 0), NonZeroU64::new(512).unwrap()),
		(2u64.pow(55) - 1, 998_046_875)
	)]
	#[case::with_nanos_rounded_up_when_10_fractional_digits_and_5_as_10th_fractional_digit_and_odd_9th_fractional_digit(
		((u64::MAX, 0), NonZeroU64::new(1024).unwrap()),
		(2u64.pow(54) - 1, 999_023_438)
	)]
	#[case::with_nanos_rounded_down_when_10_fractional_digits_and_5_as_10th_fractional_digit_and_even_9th_fractional_digit(
		((u64::MAX - 2, 0), NonZeroU64::new(1024).unwrap()),
		(2u64.pow(54) - 1, 997_070_312)
	)]
	#[case::with_nanos_rounded_up_when_10th_fractional_digit_exceeds_5(
		((u64::MAX - 1, 0), NonZeroU64::new(3).unwrap()),
		((u64::MAX / 3) - 1, 666_666_667)
	)]
	#[case::with_nanos_rounded_up_when_10th_fractional_digit_exceeds_5(
		((u64::MAX - 2, 0), NonZeroU64::new(3).unwrap()),
		((u64::MAX / 3) - 1, 333_333_333)
	)]
	fn div_by_non_zero_u64_returns_quotient(
		#[case] input: ((u64, u32), NonZeroU64),
		#[case] expected: (u64, u32),
	) {
		// Arrange
		let ((units, nanos), divisor) = input;
		let money = Money::try_new(units, nanos).unwrap();

		// Act
		let quotient = money.div_by_non_zero_u64(divisor);

		// Assert
		let expected_quotient = {
			let (units, nanos) = expected;
			Money::<USD>::try_new(units, nanos).unwrap()
		};

		assert_eq!(quotient, expected_quotient);
	}

	#[rstest]
	#[case(0, (43, 0))]
	#[case(4, (42, 987_700_000))]
	#[case(5, (42, 987_650_000))]
	#[case(9, (42, 987_654_321))]
	#[case(10, (42, 987_654_321))]
	fn saturating_round_nanos_returns_money_with_rounded_nanos_when_rounded_amount_does_not_exceed_max_money(
		#[case] dp: u8,
		#[case] expected: (u64, u32),
	) {
		// Arrange
		let money = Money::try_new(42, 987_654_321).unwrap();

		// Act
		let rounded_money = money.saturating_round_nanos(dp);

		// Assert
		let expected_money = {
			let (units, nanos) = expected;
			Money::<USD>::try_new(units, nanos).unwrap()
		};

		assert_eq!(rounded_money, expected_money);
	}

	#[test]
	fn saturating_round_nanos_returns_max_money_when_rounded_money_exceeds_max_money() {
		// Arrange
		let money = Money::<USD>::try_new(u64::MAX, 950_000_000).unwrap();
		let dp = 0;

		// Act
		let rounded_money = money.saturating_round_nanos(dp);

		// Assert
		assert_eq!(rounded_money, Money::MAX);
	}

	#[rstest]
	#[case(0, (43, 0))]
	#[case(4, (42, 987_700_000))]
	#[case(5, (42, 987_660_000))]
	#[case(9, (42, 987_654_321))]
	#[case(10, (42, 987_654_321))]
	fn saturating_round_up_nanos_returns_money_with_rounded_nanos_when_rounded_money_does_not_exceed_max_money(
		#[case] dp: u8,
		#[case] expected: (u64, u32),
	) {
		// Arrange
		let money = Money::try_new(42, 987_654_321).unwrap();

		// Act
		let rounded_money = money.saturating_round_up_nanos(dp);

		// Assert
		let expected_money = {
			let (units, nanos) = expected;
			Money::<USD>::try_new(units, nanos).unwrap()
		};

		assert_eq!(rounded_money, expected_money);
	}

	#[test]
	fn saturating_round_up_nanos_returns_max_money_when_rounded_money_exceeds_max_money() {
		// Arrange
		let money = Money::<USD>::MAX;
		let dp = 8;

		// Act
		let rounded_money = money.saturating_round_up_nanos(dp);

		// Assert
		assert_eq!(rounded_money, Money::MAX);
	}

	#[rstest]
	#[case(0, (42, 0))]
	#[case(3, (42, 123_000_000))]
	#[case(7, (42, 123_456_700))]
	#[case(9, (42, 123_456_789))]
	#[case(10, (42, 123_456_789))]
	fn round_down_nanos_works(#[case] dp: u8, #[case] expected: (u64, u32)) {
		// Arrange
		let money = Money::try_new(42, 123_456_789).unwrap();

		// Act
		let rounded_money = money.round_down_nanos(dp);

		// Assert
		let expected_money = {
			let (units, nanos) = expected;
			Money::<USD>::try_new(units, nanos).unwrap()
		};

		assert_eq!(rounded_money, expected_money);
	}

	#[test]
	fn to_amount_str_works() {
		// Arrange
		let money = Money::<USD>::try_new(100, 123_456_789).unwrap();

		// Act
		let amount_str = money.to_amount_str();

		// Assert
		let expected_amount_str = "100.123456789";

		assert_eq!(amount_str, expected_amount_str);
	}

	#[rstest]
	#[case::with_only_units_when_dp_equals_0(0, "100")]
	#[case::with_units_and_decimal_point_and_truncated_nanos_when_non_zero_dp_does_not_exceed_9(
		5,
		"100.12345"
	)]
	#[case::with_units_and_decimal_point_and_nanos_when_dp_exceeds_9(10, "100.123456789")]
	fn to_amount_str_dp_returns_display_string(#[case] dp: u8, #[case] expected_amount_str: &str) {
		// Arrange
		let money = Money::<USD>::try_new(100, 123_456_789).unwrap();

		// Act
		let amount_str = money.to_amount_str_dp(dp);

		// Assert
		assert_eq!(amount_str, expected_amount_str);
	}

	#[rstest]
	#[case::zero_money(Money::ZERO, Decimal::ZERO)]
	#[case::non_zero_money(Money::try_new(10, 83600).unwrap(), Decimal::new(100000836, 7))]
	fn decimal_from_money_works(#[case] money: Money<USD>, #[case] expected_decimal: Decimal) {
		// Act
		let decimal = Decimal::from(money);

		// Assert
		assert_eq!(decimal, expected_decimal);
	}

	#[rstest]
	#[case::zero_decimal(Decimal::ZERO, Money::ZERO)]
	#[case::postive_decimal_does_not_exceed_max_money_amount(
		Decimal::new(676103, 2),
		Money::try_new(6761, 30_000_000).unwrap())
	]
	fn money_try_from_decimal_returns_money_when_decimal_in_valid_range(
		#[case] decimal: Decimal,
		#[case] expected_money: Money<USD>,
	) {
		// Act
		let money = Money::try_from(decimal).unwrap();

		// Assert
		assert_eq!(money, expected_money);
	}

	#[rstest]
	#[case::negative_decimal(Decimal::NEGATIVE_ONE)]
	#[case::postivie_decimal_exceeds_max_money_amount(Decimal::MAX)]
	fn money_try_from_decimal_returns_error_when_decimal_not_in_valid_range(
		#[case] decimal: Decimal,
	) {
		// Act
		let money = Money::<USD>::try_from(decimal);

		// Assert
		assert!(money.is_err());
	}
}
